# H4sim
Modification of h4sim used for compelte SM simulation for TB 2018 and TB 2023 setup
Work in progress :
- TB_2018 matrix geometry : done
- TB_2023 SM geometry : done
- Beam positon and direction : done
- Table position : done
- TTree output : done
  - One branch with generator infos (1 record per event): <BR>
    "MC_infos" "Energy/F:Xtal_target/I:d_eta/F:d_phi/F:xHodo/F:yHodo/F:xDet/F:yDet/F"
  - One branch with total energy deposited in each crystal (1 record per event): <BR>
    "Crystals" "Edep[25/1700]/F"
  - One branch with hits: either dEdx or Cerenkov photons (thousands of record per event):<BR>
    "Hits_xxxx" "iXtal/I:iType/I:t/F:posX/F:posY/F:posZ/F:dirX/F:dirY/F:dirZ/F:E/F"
  - To save or not dEdx or Cerenkov photons info, you can play with <BR>/event/savedEdx or /event/saveCerenkov commands in the configuration file
  - The Cerenkov photon generation process is only switched on when you want to save the corresponding hits (/event/saveCerenkov true)
- Root macro to analyze TTree : done
  - Since we fill only the TBranch, the number of Entries of the TTree is 0.
    Depending on what you want to look at you have to set this number according to the branch you want to analyse: <BR>
    H4_tree->SetEntries(H4_tree->GetBranch("Hits")->GetEntries())
  - Have a look at read_h4sim_tree.C
- Position calibration (get Qmax position for each crystal): To be done
- Intercalibration (the angle of crystal is different for each position): To be done
- Linearity (due to different angles of adjacent crystals): To be done

To use it (for TB 2018) on lxplus:
- Clone the git repository
- cd h4sim; cp CMakeLists.txt.TB_2018 CMakeLists.txt
- source setup.sh
- cd ..; mkdir TB_2018_build; cd TB_2018_build
- cmake ../h4sim
- make

Then, have a look at vis.mac to set either TB_2018 or TB_2023 parameters and launch the program:
- ./H4_simul
- The simulation results are written in "output_dir". You should have this directory created or a linked to a place where you have some space.

- For TB_2018, we move the beam in X Y poisition <BR>
  - /H4/SetPosXY xpos ypos unit (e.g. /H4/SetPosXY 6.2  7.8 cm for C3) <BR>
    pos 0. 0. is upper-right corner (with the beam in your back) <BR>
  - /run/beamOn 1

- For TB_2023, you select eta and phi indices <BR>
/H4/SetPosEtaPhi i j (e.g. /H4/SetPosEtaPhi 12 12 for channel ?)

- Warning : <BR>
You have to select the right gcc/geant4/root versions. Have a look in setup.sh to see if it is corresponding to the current version of lxplus<BR><BR>
Creation : M.D. 2018/05/29 <BR>
Last mod : M.D. 2024/11/20
