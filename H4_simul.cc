#include "G4RunManager.hh"
#include "G4RunManagerFactory.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "Randomize.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "VisManager.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
//#include "SteppingVerbose.hh"


using namespace std;
void usage() {
  cout << "super [config_file]"<< endl;
  exit(-1);
}

int main(int argc,char** argv) 
{
  // detect interactive mode (if no arguments) and define UI session
  G4UIExecutive* ui = nullptr;
  if (argc == 1)
  {
    ui = new G4UIExecutive(argc, argv);
  }

  // construct a serial run manager
  auto* runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::SerialOnly);
  // construct a MultiThreaded run manager
  //auto* runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::MT);

  G4String output ;
  G4String conf_file ;
  G4String input_path ;
  input_path = "xml" ;

  // Verbose output class
  //G4VSteppingVerbose::SetInstance(new SteppingVerbose);

  // set mandatory initialization classes
  DetectorConstruction* detector = new DetectorConstruction();
  runManager->SetUserInitialization(detector);
  G4int CerenkovOn=0;
  G4long seed=1;
  if(argc==2)
  {
// If we are in batch mode, look if we request Cerenkov generation
    FILE *fconf=fopen(argv[1],"r");
    if(!fconf)
    {
      printf("Config file not found. Stop here\n");
      exit;
    }
    char *line=NULL;
    size_t len=0;
    int eof;
    while ((eof=getline(&line, &len, fconf)) != EOF)
    {
      if(line[0]=='#') continue;

      if(strstr(line,"/H4/RndmSeed"))
      {
        sscanf(line,"/H4/RndmSeed %ld",&seed);
      }

      if(strstr(line,"saveCerenkov"))
      {
        if(strstr(line,"true") || strstr(line,"TRUE"))
        {
          CerenkovOn=1;
          printf("Switch Cerenkov generator ON\n");
        }
        //break;
      }
    }
    fclose(fconf);
    delete(line);
  }
  printf("Random generator seed found : %d\n",seed);
  PhysicsList *physlist = new PhysicsList(CerenkovOn);
  runManager->SetUserInitialization(physlist);
 
  char confname[132],rootname[132],*fname;
  if (ui)
  {
    sprintf(rootname,"TB_2018.root");
  }
  else
  {
    strcpy(confname, argv[1]);
    char *sep=strrchr(argv[1],'/');
    if(!sep)
      fname=strsep(&argv[1],".");
    else
    {
      sep=&sep[1];
      fname=strsep(&sep,".");
    }
    printf("name found : %s\n",fname);
    sprintf(rootname,"output_dir/TB_2018_%s.root",fname);
    printf("Root file name : %s\n",rootname);
  }
  TFile *hFile = new TFile(rootname, "RECREATE","H4 Geant4 simulation");
  TTree *myTree = new TTree("G4_tree","H4 simulation results");
  // Choose the Random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  // Set Seed
  CLHEP::HepRandom::setTheSeed(seed);
  cout << "Random Seed: " << CLHEP::HepRandom::getTheSeed() << endl;

    // set user action classes 
  PrimaryGeneratorAction* kin = new PrimaryGeneratorAction(detector);

  runManager->SetUserAction(kin);
  runManager->SetUserAction(new RunAction);
  runManager->SetUserAction(new EventAction(myTree,detector,kin));
  runManager->SetUserAction(new SteppingAction);
    
  // initialize visualization
  G4VisManager* visManager = nullptr;

  //Initialize G4 kernel
  runManager->Initialize(); 

  // get the pointer to the User Interface manager 
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  if (ui) {
    // interactive mode
    printf("Start interactive session\n");
    visManager = new G4VisExecutive;
    visManager->Initialize();
    ui->SessionStart();
    delete ui;
  }
  else {
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = confname;
    UImanager->ApplyCommand(command + fileName);
  }

  //job termination
  //
  myTree->Write();
  hFile->Close();;
  delete visManager;
  delete runManager;
}
