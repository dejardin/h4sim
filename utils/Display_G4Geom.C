#include <TSystem.h>
#include <TDirectory.h>
#include <TF2.h>
#include <TFile.h>
#include <TStyle.h>
#include <TGeoManager.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoBBox.h>
#include <TGeoTube.h>
#include <TGeoArb8.h>
#include <TGeoCompositeShape.h>
#include "TEveManager.h"
#include "TEvePlot3D.h"
#include "TEvePointSet.h"
#include "TEveGeoNode.h"
#include "TGLViewer.h"
#include "TGLUtil.h"

// Small root macro to display detector geometry used for simulation and SLitrani
// Convert G4 geometry to root geometry.
// Used as input for SLItrani geometry building
// xml files have been simplified to be easily parsed. (.xml -> .def)
void Display_G4Geom(TString evt_file="", Int_t ievt=1, Int_t target_location=12, Int_t period=2018) // Look at C3 by default (stack 1, Alveola 2, crystal 3)
{
  Int_t num_alveola[25]={2,1,1,0,0,2,1,1,0,0,2,1,1,0,0,2,1,1,0,0,2,1,1,0,0};
  Int_t num_Xtal[25]   ={0,5,0,5,0,1,6,1,6,1,2,7,2,7,2,3,8,3,8,3,4,9,4,9,4};
  Int_t target_stack=0;
  Int_t target_alveola=num_alveola[target_location];
  Int_t target_Xtal=num_Xtal[target_location];
  TString targetX, targetXS;
  targetX =TString::Format("S%2.2d_A%2.2d_X%2.2d",target_stack+1,target_alveola+1,target_Xtal+1);
  targetXS=TString::Format("S%2.2d_A%2.2d_XS%2.2d",target_stack+1,target_alveola+1,target_Xtal+1);
  Int_t iXnode_ref=0, iAnode_ref=0;
  TGeoNode *Xnode_ref, *Anode_ref;
  Bool_t ref_found=kFALSE;
  Double_t center_ref[3]={0.};

  TCanvas *c1=new TCanvas();
  TGLViewer *view=(TGLViewer*)gPad->GetViewer3D();
  char geo_name[80], xtal_name[80], alveola_name[80], rot_name[80];

  sprintf(rot_name,"xml/rotations.def");
  if(period!=2018)
  {
    sprintf(geo_name,"xml/ecal.def");
    sprintf(xtal_name,"xml/crystals.def");
    sprintf(alveola_name,"xml/alveolas.def");
  }
  else
  {
    sprintf(geo_name,"xml/ecal_2018.def");
    sprintf(xtal_name,"xml/crystals_2018.def");
    sprintf(alveola_name,"xml/alveolas_2018.def");
  }
  Color_t  VacuumColor     =  0;
  Color_t  EdepColor       =  kMagenta;
  Color_t  PbWO4Color      =  kBlue;
  Color_t  DepolishedColor =  kRed;
  Color_t  PlasticColor    =  kYellow;
  Color_t  AluColor        =  kGray;
  Color_t  GlueColor       =  kCyan;
  Color_t  TotAbsColor     =  kBlack;
  Color_t  SiliciumColor   =  kGreen;
  Color_t  SourceColor     =  5;
  Double_t IrrA            = 0.0; //Value used for A   when irrelevant !
  Double_t IrrZ            = 0.0; //Value used for Z   when irrelevant !
  Double_t IrrRho          = 0.0; //Value used for Rho when irrelevant !
  Double_t Air_RefrIndex     = 1.0003; //Refractive index of air
  Int_t MediumIndex = 1;

  char *line=NULL;
  size_t len=0;

//____________________________________________________________________________
//
// Building the geometry
  TGeoManager *geom = new TGeoManager("setup","G4Geom setup");
  TGeoMaterial *vacuum_mat = new TGeoMaterial("Vacuum",IrrA,IrrZ,IrrRho);
  TGeoMedium *vacuum = new TGeoMedium("vacuum",MediumIndex++, vacuum_mat);
// top box containing everything and defining the WCS, world coordinate system
  Double_t top_dx    = 300.0;
  Double_t top_dy    = 300.0;
  Double_t top_dz    = 300.0;
  TGeoVolume *top = geom->MakeBox("TOP",vacuum,top_dx,top_dy,top_dz);
  geom->SetTopVolume(top);

  Double_t PbWO4_radlen   = 0.893;//radiation length of PbWO4
  Double_t PbWO4_intlen   = 19.5; //interaction length of PbWO4
  Double_t PbWO4_rMoliere = 2.0;  //Moliere radius of PbWO4 in cm
  TGeoElementTable *table = gGeoManager->GetElementTable();
  TGeoElement *Pb = table->FindElement("LEAD");
  TGeoElement *W  = table->FindElement("TUNGSTEN");
  TGeoElement *O  = table->FindElement("OXYGEN");
  TGeoMixture *pbwo4_mix = new TGeoMixture("PbWO4_mix",3,8.28);
  pbwo4_mix->AddElement(Pb,1);
  pbwo4_mix->AddElement(W,1);
  pbwo4_mix->AddElement(O,4);  
  pbwo4_mix->SetRadLen(-PbWO4_radlen,PbWO4_intlen);
  TGeoMedium *pbwo4_med = new TGeoMedium("PbWO4_MED",MediumIndex++,pbwo4_mix);
  TGeoElement *Al = table->FindElement("ALUMINIUM");
  TGeoMaterial *aluminium_mat = new TGeoMaterial("Aluminium",Al,1.);
  TGeoMedium *aluminium = new TGeoMedium("aluminium",MediumIndex++,aluminium_mat);
  TGeoMaterial *plastic_mat = new TGeoMaterial("Plastic",IrrA,IrrZ,IrrRho);
  TGeoMedium *plastic = new TGeoMedium("plastic",MediumIndex++,plastic_mat);
  TGeoElement *Si = table->FindElement("SILICON");
  TGeoMaterial *silicium_mat = new TGeoMaterial("Silicium",Si,2.33);
  TGeoMedium *silicium = new TGeoMedium("Silicium",MediumIndex++,silicium_mat);
  TGeoMaterial *glue_mat = new TGeoMaterial("Glue",IrrA,IrrZ,IrrRho);
  TGeoMedium *glue = new TGeoMedium("glue",MediumIndex++,glue_mat);
  TGeoMaterial *epoxy_mat = new TGeoMaterial("Epoxy",IrrA,IrrZ,IrrRho);
  TGeoMedium *epoxy = new TGeoMedium("epoxy",MediumIndex++,epoxy_mat);
  TGeoMaterial *fiber_mat = new TGeoMaterial("Fiber",IrrA,IrrZ,IrrRho);
  TGeoMedium *fiber_core = new TGeoMedium("fiber_core",MediumIndex++,fiber_mat);
  TGeoElement *Fe = table->FindElement("IRON");
  TGeoMaterial *Fe_mat = new TGeoMaterial("Iron",Fe,1.);
  TGeoMedium *MCP_med = new TGeoMedium("MCP",MediumIndex++,Fe_mat);

  Double_t depolish_thickness=0.005; // slice of isotropic PbWO4 for depolishing face : 50 um

  TGeoVolume *alveola[170];
  TGeoRotation *Arot[170];
  TGeoCombiTrans *Acombi[170];
  TGeoVolume *crystal[1700];
  TGeoVolume *crystal_side[1700];
  TGeoVolume *capsule[1700];
  TGeoVolume *wrapping[1700];
  TGeoVolume *porte_ferule[1700];
  TGeoVolume *fiber[1700];
  TGeoRotation *Xrot[1700], *Crot[1700];
  TGeoCombiTrans *Xcombi[1700], *Ccombi[1700];
  Double_t deg=asin(1.0l)/90.0l;
  TGeoVolume *MCP;

  Int_t alveola_type[17];
  Double_t alveola_geo[17][10][11];
  Double_t alveola_pos[17][10][9];
  Double_t Xtal_geo[17][10][11];
  Double_t Xtal_pos[17][10][9];
  TGeoHMatrix *current_global_matrix;

  FILE *fgeom=fopen(geo_name,"r");
  Int_t nStack[17]={0}, nAlveola=-1, nXtal[17]={0}, nFerrule[17]={0};
  Int_t Geof=1;
  char srot[80], Aname[80], Aname_ref[80], Xname[80], XSname[80], Rname[80];
  sprintf(Aname_ref,"");
  while ((Geof=getline(&line, &len, fgeom)) != EOF) 
  {
    char name[80];
    sscanf(line,"%s",name);
    printf("name read : %s\n",name);
    if(strstr(name,"_XT")!=NULL) // Xtal
    {
//ALV01_XT01      E165 139.146  22.4155 -145.447
      sscanf(line,"%s %s %lf %lf %lf",name,srot,&Xtal_pos[nAlveola][nXtal[nAlveola]][6],&Xtal_pos[nAlveola][nXtal[nAlveola]][7],
                                                &Xtal_pos[nAlveola][nXtal[nAlveola]][8]);
// Search the rotation
      FILE *frot=fopen(rot_name,"r");
      Int_t Reof=1;
      while((Reof=getline(&line, &len, frot)) != EOF)
      {
        sscanf(line,"%s",Rname);
        if(strcmp(Rname,srot)==0)break;
      }
      if(Reof==EOF)printf("Rotation %s not found\n",srot);
      fclose(frot);
      sscanf(line,"%s %lf %lf %lf %lf %lf %lf",Rname,&Xtal_pos[nAlveola][nXtal[nAlveola]][0],&Xtal_pos[nAlveola][nXtal[nAlveola]][1],
                                                     &Xtal_pos[nAlveola][nXtal[nAlveola]][2],&Xtal_pos[nAlveola][nXtal[nAlveola]][3],
                                                     &Xtal_pos[nAlveola][nXtal[nAlveola]][4],&Xtal_pos[nAlveola][nXtal[nAlveola]][5]);
// Search for Xtal geometry
      FILE *fgeo=fopen(xtal_name,"r");
      Int_t Xeof=1;
      while((Xeof=getline(&line, &len, fgeo)) != EOF)
      {
        sscanf(line,"%s",Xname);
        if(strcmp(Xname,name)==0)break;
      }
      if(Xeof==EOF)printf("Xtal %s not found\n",name);
      fclose(fgeo);
      sscanf(line,"%s %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",name,&Xtal_geo[nAlveola][nXtal[nAlveola]][0],&Xtal_geo[nAlveola][nXtal[nAlveola]][1],
                                                                        &Xtal_geo[nAlveola][nXtal[nAlveola]][2],&Xtal_geo[nAlveola][nXtal[nAlveola]][3],
                                                                        &Xtal_geo[nAlveola][nXtal[nAlveola]][4],&Xtal_geo[nAlveola][nXtal[nAlveola]][5],
                                                                        &Xtal_geo[nAlveola][nXtal[nAlveola]][6],&Xtal_geo[nAlveola][nXtal[nAlveola]][7],
                                                                        &Xtal_geo[nAlveola][nXtal[nAlveola]][8],&Xtal_geo[nAlveola][nXtal[nAlveola]][9],
                                                                        &Xtal_geo[nAlveola][nXtal[nAlveola]][10]);
      nXtal[nAlveola]++;
    }
    else if(strstr(name,"_FER")!=NULL) // Porte-ferrules
    {
    }
    else // Alveola
    {
      if(strcmp(name,Aname_ref)!=0) // New Alveola
      {
        strcpy(Aname_ref,name);
        nAlveola++;
        sscanf(name,"ALV%d",&alveola_type[nAlveola]);
        nStack[nAlveola]=0;
        nXtal[nAlveola]=0;
        nFerrule[nAlveola]=0;
      }
      printf("Read alveola %d properties : type %d\n",nAlveola,alveola_type[nAlveola]);
//ALV01      E165 139.146  22.4155 -145.447
      sscanf(line,"%s %s %lf %lf %lf",name,srot,&alveola_pos[nAlveola][nStack[nAlveola]][6],&alveola_pos[nAlveola][nStack[nAlveola]][7],
                                                &alveola_pos[nAlveola][nStack[nAlveola]][8]);
      //alveola_pos[nAlveola][nStack[nAlveola]][6]-=120.;
// Search the rotation
      FILE *frot=fopen(rot_name,"r");
      Int_t Reof=1;
      char Rname[80];
      while((Reof=getline(&line, &len, frot)) != EOF)
      {
        sscanf(line,"%s",Rname);
        if(strcmp(Rname,srot)==0)break;
      }
      if(Reof==EOF)printf("Rotation %s not found\n",srot);
      fclose(frot);
      sscanf(line,"%s %lf %lf %lf %lf %lf %lf",Rname,&alveola_pos[nAlveola][nStack[nAlveola]][0],&alveola_pos[nAlveola][nStack[nAlveola]][1],
                                                     &alveola_pos[nAlveola][nStack[nAlveola]][2],&alveola_pos[nAlveola][nStack[nAlveola]][3],
                                                     &alveola_pos[nAlveola][nStack[nAlveola]][4],&alveola_pos[nAlveola][nStack[nAlveola]][5]);
// Search for Alveola geometry
      FILE *fgeo=fopen(alveola_name,"r");
      Int_t Aeof=1;
      while((Aeof=getline(&line, &len, fgeo)) != EOF)
      {
        sscanf(line,"%s",Aname);
        if(strcmp(Aname,name)==0)break;
      }
      if(Aeof==EOF)printf("Alveola %s not found\n",name);
      fclose(fgeo);
      sscanf(line,"%s %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",name,&alveola_geo[nAlveola][nStack[nAlveola]][0],&alveola_geo[nAlveola][nStack[nAlveola]][1],
                                                                        &alveola_geo[nAlveola][nStack[nAlveola]][2],&alveola_geo[nAlveola][nStack[nAlveola]][3],
                                                                        &alveola_geo[nAlveola][nStack[nAlveola]][4],&alveola_geo[nAlveola][nStack[nAlveola]][5],
                                                                        &alveola_geo[nAlveola][nStack[nAlveola]][6],&alveola_geo[nAlveola][nStack[nAlveola]][7],
                                                                        &alveola_geo[nAlveola][nStack[nAlveola]][8],&alveola_geo[nAlveola][nStack[nAlveola]][9],
                                                                        &alveola_geo[nAlveola][nStack[nAlveola]][10]);
      nStack[nAlveola]++;
    }
  }
  nAlveola++;
    
  Int_t inode=0;
  Int_t iXnode=0;
  Int_t iAnode=0;
  for(Int_t iAlveola=0; iAlveola<nAlveola; iAlveola++)
  {
    for(Int_t iStack=0; iStack<nStack[iAlveola]; iStack++)
    {
      sprintf(Aname,"S%2.2d_A%2.2d",iStack+1,iAlveola+1);
      printf("Making Alveola %d : %s\n",iAnode,Aname);
      alveola[iAnode] = geom->MakeTrap(Aname,pbwo4_med,alveola_geo[iAlveola][iStack][0],alveola_geo[iAlveola][iStack][10],alveola_geo[iAlveola][iStack][9],
                                                       alveola_geo[iAlveola][iStack][4],alveola_geo[iAlveola][iStack][2],
                                                       alveola_geo[iAlveola][iStack][3],alveola_geo[iAlveola][iStack][1],
                                                       alveola_geo[iAlveola][iStack][8],alveola_geo[iAlveola][iStack][6],
                                                       alveola_geo[iAlveola][iStack][7],alveola_geo[iAlveola][iStack][5]);
      alveola[iAnode]->SetVisibility(kTRUE);
      alveola[iAnode]->SetLineColor(PbWO4Color);
      alveola[iAnode]->SetLineWidth(2);

      Double_t tx=alveola_pos[iAlveola][iStack][0], px=alveola_pos[iAlveola][iStack][1];
      Double_t ty=alveola_pos[iAlveola][iStack][2], py=alveola_pos[iAlveola][iStack][3];
      Double_t tz=alveola_pos[iAlveola][iStack][4], pz=alveola_pos[iAlveola][iStack][5];
      Double_t dx=alveola_pos[iAlveola][iStack][6], dy=alveola_pos[iAlveola][iStack][7], dz=alveola_pos[iAlveola][iStack][8]+152.;
      printf("Alveola translation : %.5f %.5f %.5f\n",dx,dy,dz);
      sprintf(Rname,"R_S%2.2d_A%2.2d",iStack+1,iAlveola+1);
      Arot[iAnode]=new TGeoRotation(Rname,tx,px,ty,py,tz,pz);
      if(period==2018)
      {
// For TB 2018, the bottom of the second alveola is parallel to horizontal
        Arot[iAnode]->RotateZ(-1.);
// And, the first column of crystals was parallel to beam line
        Arot[iAnode]->RotateY(21.7);
      }
      Acombi[iAnode]=new TGeoCombiTrans(dx,dy,dz,Arot[iAnode]);

      top->AddNode(alveola[iAnode],iAnode,Acombi[iAnode]);

      Int_t inode_loc=0;
      for(Int_t iXtal=0; iXtal<nXtal[iAlveola]; iXtal++)
      {
  // dz   alp1           bl1      tl1      h1        alp2           bl2      tl2      h2       phi          theta
        Double_t crystal_dz=Xtal_geo[iAlveola][iXtal][0];
        sprintf(Xname,"S%2.2d_A%2.2d_X%2.2d",iStack+1,iAlveola+1,iXtal+1);
        printf("Making Xtal %d : %s\n",iXnode,Xname);
        Double_t vertex[8][3], loc_vertex[8][3], global_vertex[8][3], center[2][2];
        Double_t ttx  = TMath::Tan(Xtal_geo[iAlveola][iXtal][10]*deg) * TMath::Cos(Xtal_geo[iAlveola][iXtal][9]*deg);
        Double_t tty  = TMath::Tan(Xtal_geo[iAlveola][iXtal][10]*deg) * TMath::Sin(Xtal_geo[iAlveola][iXtal][9]*deg);
        Double_t tta1 = TMath::Tan(Xtal_geo[iAlveola][iXtal][1]*deg);
        Double_t tta2 = TMath::Tan(Xtal_geo[iAlveola][iXtal][5]*deg);
        vertex[0][0] = -crystal_dz * ttx - Xtal_geo[iAlveola][iXtal][4] * tta1 - Xtal_geo[iAlveola][iXtal][2];
        vertex[0][1] = -crystal_dz * tty - Xtal_geo[iAlveola][iXtal][4];
        vertex[0][2] = -crystal_dz;
        vertex[1][0] = -crystal_dz * ttx + Xtal_geo[iAlveola][iXtal][4] * tta1 - Xtal_geo[iAlveola][iXtal][3];
        vertex[1][1] = -crystal_dz * tty + Xtal_geo[iAlveola][iXtal][4];
        vertex[1][2] = -crystal_dz;
        vertex[2][0] = -crystal_dz * ttx + Xtal_geo[iAlveola][iXtal][4] * tta1 + Xtal_geo[iAlveola][iXtal][3];
        vertex[2][1] = -crystal_dz * tty + Xtal_geo[iAlveola][iXtal][4];
        vertex[2][2] = -crystal_dz;
        vertex[3][0] = -crystal_dz * ttx - Xtal_geo[iAlveola][iXtal][4] * tta1 + Xtal_geo[iAlveola][iXtal][2];
        vertex[3][1] = -crystal_dz * tty - Xtal_geo[iAlveola][iXtal][4];
        vertex[3][2] = -crystal_dz;
        vertex[4][0] =  crystal_dz * ttx - Xtal_geo[iAlveola][iXtal][8] * tta2 - Xtal_geo[iAlveola][iXtal][6];
        vertex[4][1] =  crystal_dz * tty - Xtal_geo[iAlveola][iXtal][8];
        vertex[4][2] =  crystal_dz;
        vertex[5][0] =  crystal_dz * ttx + Xtal_geo[iAlveola][iXtal][8] * tta2 - Xtal_geo[iAlveola][iXtal][7];
        vertex[5][1] =  crystal_dz * tty + Xtal_geo[iAlveola][iXtal][8];
        vertex[5][2] =  crystal_dz;
        vertex[6][0] =  crystal_dz * ttx + Xtal_geo[iAlveola][iXtal][8] * tta2 + Xtal_geo[iAlveola][iXtal][7];
        vertex[6][1] =  crystal_dz * tty + Xtal_geo[iAlveola][iXtal][8];
        vertex[6][2] =  crystal_dz;
        vertex[7][0] =  crystal_dz * ttx - Xtal_geo[iAlveola][iXtal][8] * tta2 + Xtal_geo[iAlveola][iXtal][6];
        vertex[7][1] =  crystal_dz * tty - Xtal_geo[iAlveola][iXtal][8];
        vertex[7][2] =  crystal_dz;
        center[0][0] = (vertex[0][0]+vertex[1][0]+vertex[2][0]+vertex[3][0])/4.;
        center[0][1] = (vertex[0][1]+vertex[1][1]+vertex[2][1]+vertex[3][1])/4.;
        center[1][0] = (vertex[4][0]+vertex[5][0]+vertex[6][0]+vertex[7][0])/4.;
        center[1][1] = (vertex[4][1]+vertex[5][1]+vertex[6][1]+vertex[7][1])/4.;
        Double_t BF=vertex[1][1]-vertex[0][1];                           // For Right crystals
        if(Xtal_geo[iAlveola][iXtal][1]<0) BF=vertex[2][1]-vertex[3][1]; // Left crystals
        Double_t AF=vertex[3][0]-vertex[0][0];
        Double_t CF=vertex[2][0]-vertex[1][0];

        for(Int_t i=0; i<8; i++)
        {
          loc_vertex[i][0]=vertex[i][0];
          loc_vertex[i][1]=vertex[i][1];
          loc_vertex[i][2]=vertex[i][2];
        }
        if(Xtal_geo[iAlveola][iXtal][1]>0) // Right crystals
        {
          loc_vertex[2][0]-=depolish_thickness;
          loc_vertex[3][0]-=depolish_thickness;
          loc_vertex[6][0]-=depolish_thickness;
          loc_vertex[7][0]-=depolish_thickness;
        }
        else                               // Left crystals
        {
          loc_vertex[0][0]+=depolish_thickness;
          loc_vertex[1][0]+=depolish_thickness;
          loc_vertex[4][0]+=depolish_thickness;
          loc_vertex[5][0]+=depolish_thickness;
        }
        Double_t coord[16];
        for(int i=0; i<8; i++)
        {
          coord[2*i]=loc_vertex[i][0];
          coord[2*i+1]=loc_vertex[i][1];
        }

        Double_t tx=Xtal_pos[iAlveola][iXtal][0], px=Xtal_pos[iAlveola][iXtal][1];
        Double_t ty=Xtal_pos[iAlveola][iXtal][2], py=Xtal_pos[iAlveola][iXtal][3];
        Double_t tz=Xtal_pos[iAlveola][iXtal][4], pz=Xtal_pos[iAlveola][iXtal][5];
        Double_t dx=Xtal_pos[iAlveola][iXtal][6], dy=Xtal_pos[iAlveola][iXtal][7], dz=Xtal_pos[iAlveola][iXtal][8];
        Double_t XX = sin (tx*deg)*cos(px*deg);
        Double_t XY = sin (ty*deg)*cos(py*deg);
        Double_t XZ = sin (tz*deg)*cos(pz*deg);
        Double_t YX = sin (tx*deg)*sin(px*deg);
        Double_t YY = sin (ty*deg)*sin(py*deg);
        Double_t YZ = sin (tz*deg)*sin(pz*deg);
        Double_t ZX = cos(tx*deg);
        Double_t ZY = cos(ty*deg);
        Double_t ZZ = cos(tz*deg);

        printf("Xtal rotation    : X(%.5f, %.5f, %.5f) Y(%.5f, %.5f, %.5f) Z(%.5f, %.5f, %.5f)\n",XX,YX,ZX,XY,YY,ZY,XZ,YZ,ZZ);
        printf("Xtal translation : %.5f %.5f %.5f\n",dx,dy,dz);
        sprintf(Rname,"R_S%2.2d_A%2.2d_X%2.2d",iStack+1,iAlveola+1,iXtal+1);
        Xrot[iXnode]=new TGeoRotation(Rname,tx,px,ty,py,tz,pz);
        Xcombi[iXnode]=new TGeoCombiTrans(dx,dy,dz,Xrot[iXnode]);

        crystal[iXnode] = geom->MakeArb8(Xname,pbwo4_med,crystal_dz,coord);
        //crystal[iXnode] = geom->MakeTrap(name,pbwo4_med,Xtal_geo[iAlveola][iXtal][0],Xtal_geo[iAlveola][iXtal][10],Xtal_geo[iAlveola][iXtal][9],
        //                                                Xtal_geo[iAlveola][iXtal][4],Xtal_geo[iAlveola][iXtal][2],
        //                                                Xtal_geo[iAlveola][iXtal][3],Xtal_geo[iAlveola][iXtal][1],
        //                                                Xtal_geo[iAlveola][iXtal][8],Xtal_geo[iAlveola][iXtal][6],
        //                                                Xtal_geo[iAlveola][iXtal][7],Xtal_geo[iAlveola][iXtal][5]);
        crystal[iXnode]->SetVisibility(kTRUE);
        crystal[iXnode]->SetTransparency(1);
        crystal[iXnode]->SetLineColor(PbWO4Color);
        crystal[iXnode]->SetLineWidth(2);
        alveola[iAnode]->AddNode(crystal[iXnode],inode_loc,Xcombi[iXnode]);
        //inode_loc++;

// Depolished side :
        for(Int_t i=0; i<8; i++)
        {
          loc_vertex[i][0]=vertex[i][0];
          loc_vertex[i][1]=vertex[i][1];
          loc_vertex[i][2]=vertex[i][2];
        }
        if(Xtal_geo[iAlveola][iXtal][1]>0) // Right crystals
        {
          loc_vertex[0][0]=loc_vertex[3][0]-depolish_thickness;
          loc_vertex[1][0]=loc_vertex[2][0]-depolish_thickness;
          loc_vertex[4][0]=loc_vertex[7][0]-depolish_thickness;
          loc_vertex[5][0]=loc_vertex[6][0]-depolish_thickness;
        }
        else                               // Left crystals
        {
          loc_vertex[3][0]=loc_vertex[0][0]+depolish_thickness;
          loc_vertex[2][0]=loc_vertex[1][0]+depolish_thickness;
          loc_vertex[7][0]=loc_vertex[4][0]+depolish_thickness;
          loc_vertex[6][0]=loc_vertex[5][0]+depolish_thickness;
        }
        for(int i=0; i<8; i++)
        {
          coord[2*i]=loc_vertex[i][0];
          coord[2*i+1]=loc_vertex[i][1];
        }
        sprintf(XSname,"S%2.2d_A%2.2d_XS%2.2d",iStack+1,iAlveola+1,iXtal+1);
        crystal_side[iXnode] = geom->MakeArb8(XSname,pbwo4_med,crystal_dz,coord);
        crystal_side[iXnode]->SetLineColor(DepolishedColor);
        crystal_side[iXnode]->SetTransparency(1);
        crystal_side[iXnode]->SetLineWidth(2);
        alveola[iAnode]->AddNode(crystal_side[iXnode],inode_loc,Xcombi[iXnode]);
        crystal_side[iXnode]->SetVisibility(kTRUE);
        inode_loc++;

// Capsule is glued in the corner of point 0/R (3/L), 2 APDs 5x5 mm separated by 4.2 mm
// Dimensions of capsule :
        Double_t capsule_dy   = 1.15; //length of the capsule, parallel to BR (23 mm) [B. Ille]
        Double_t capsule_dx   = 1.10; //width of the capsule, parallel to CR (22 mm) [B. Ille]
        Double_t capsule_dz   = 0.10; //thickness the capsule, parallel (2 mm ?)
        Double_t dx_caps_crys = 0.12; //distance from edge of capsule to edge BR [B. Ille]
        Double_t dy_caps_crys = 0.12; //distance from edge of capsule to edge CR [B. Ille]
// Dimensions of APD
        Double_t apd_dx = 0.25;
        Double_t apd_dy = 0.25;
        Double_t apd_sep_dx = 0.21;
        Double_t apd_dz = 0.05;
// Dimensions for epoxy window of APD
        Double_t window_apd_dx = apd_dx;
        Double_t window_apd_dy = apd_dy;
        Double_t window_apd_dz = 0.015;
// Dimensions for glue of APD
        Double_t glue_apd_dx = apd_dx;
        Double_t glue_apd_dy = apd_dy;
        Double_t glue_apd_dz = 0.015;

        char nameCF[80], nameCH[80], nameC[80]; // Full capsule, APD hole and operation result
        char nameCS[80], nameOP[80];            // Capsule shape, boolean operation
        char nameCT1[80], nameCT2[80];          // APD 1 and 2 translation
        sprintf(nameCF,"S%2.2d_A%2.2d_CF%2.2d",iStack+1,iAlveola+1,iXtal+1); // full capsule
        sprintf(nameCH,"S%2.2d_A%2.2d_CH%2.2d",iStack+1,iAlveola+1,iXtal+1); // APD hole1
        sprintf(nameCS,"S%2.2d_A%2.2d_CS%2.2d",iStack+1,iAlveola+1,iXtal+1); // composite shape
        sprintf(nameC,"S%2.2d_A%2.2d_C%2.2d",iStack+1,iAlveola+1,iXtal+1);   // final capsule
        sprintf(nameCT1,"S%2.2d_A%2.2d_CT1%2.2d",iStack+1,iAlveola+1,iXtal+1);   // final capsule
        sprintf(nameCT2,"S%2.2d_A%2.2d_CT2%2.2d",iStack+1,iAlveola+1,iXtal+1);   // final capsule
        sprintf(nameOP,"%s-(%s:%s+%s:%s)",nameCF,nameCH,nameCT1,nameCH,nameCT2);// boolean operation
// First build a solid capsule :
        TGeoBBox *solid_capsule = new TGeoBBox(nameCF,capsule_dx,capsule_dy, capsule_dz);
// Make holes for the APDs :
        TGeoBBox *apd_hole = new TGeoBBox(nameCH,apd_dx,apd_dy,apd_dz+window_apd_dz+glue_apd_dz);
        TGeoTranslation *t_apd_hole1 = new TGeoTranslation(nameCT1,-apd_sep_dx-apd_dx,0.,-capsule_dz+apd_dz+window_apd_dz+glue_apd_dz-0.0001);
        TGeoTranslation *t_apd_hole2 = new TGeoTranslation(nameCT2,+apd_sep_dx+apd_dx,0.,-capsule_dz+apd_dz+window_apd_dz+glue_apd_dz-0.0001);
        t_apd_hole1->RegisterYourself();
        t_apd_hole2->RegisterYourself();
        TGeoCompositeShape *scapsule = new TGeoCompositeShape(nameCS,nameOP);
        capsule[iXnode]= new TGeoVolume(nameC,scapsule,plastic);
// Reference vetex for positionning capsule
        TGeoTranslation *t_capsule;
        if(Xtal_geo[iAlveola][iXtal][1]>0) // Right crystals
          t_capsule = new TGeoTranslation("t_capsule",(vertex[5][0]-vertex[1][0])/2.+dx_caps_crys,(vertex[5][1]-vertex[1][1])/2.-dy_caps_crys,crystal_dz+capsule_dz);
        else                               // Left crystals
          t_capsule = new TGeoTranslation("t_capsule",(vertex[6][0]-vertex[2][0])/2.-dx_caps_crys,(vertex[6][1]-vertex[2][1])/2.-dy_caps_crys,crystal_dz+capsule_dz);
        t_capsule->RegisterYourself();
        crystal[iXnode]->AddNode(capsule[iXnode],1,t_capsule);
        capsule[iXnode]->SetLineColor(PlasticColor);
        capsule[iXnode]->SetLineWidth(2);
        capsule[iXnode]->SetVisibility(kTRUE);

// Build APD and set its characteristics
        TGeoVolume *apd_det = geom->MakeBox("APD",silicium,apd_dx,apd_dy,apd_dz);
        TGeoTranslation *t_apd1 = new TGeoTranslation("t_apd1",-apd_sep_dx-apd_dx,0., -capsule_dz+(window_apd_dz+glue_apd_dz)*2.+apd_dz);
        TGeoTranslation *t_apd2 = new TGeoTranslation("t_apd2",+apd_sep_dx+apd_dx,0., -capsule_dz+(window_apd_dz+glue_apd_dz)*2.+apd_dz);
        t_apd1->RegisterYourself();
        t_apd2->RegisterYourself();
        apd_det->SetLineColor(SiliciumColor);
        capsule[iXnode]->AddNode(apd_det,1,t_apd1);
        capsule[iXnode]->AddNode(apd_det,2,t_apd2);
        apd_det->SetVisibility(kTRUE);

// put a window on the APDs
        TGeoVolume *apd_window = geom->MakeBox("APD_window",epoxy,window_apd_dx,window_apd_dy,window_apd_dz);
        TGeoTranslation *t_window1 = new TGeoTranslation("t_window1",-apd_sep_dx-apd_dx,0., -capsule_dz+window_apd_dz+2.*glue_apd_dz);
        TGeoTranslation *t_window2 = new TGeoTranslation("t_window2",+apd_sep_dx+apd_dx,0., -capsule_dz+window_apd_dz+2.*glue_apd_dz);
        apd_window->SetLineColor(SiliciumColor+2);
        t_window1->RegisterYourself();
        t_window2->RegisterYourself();
        capsule[iXnode]->AddNode(apd_window,3,t_window1);
        capsule[iXnode]->AddNode(apd_window,4,t_window2);
        apd_window->SetVisibility(kTRUE);

// put some glue below the APDs
        TGeoVolume *apd_glue = geom->MakeBox("APD_glue",glue,glue_apd_dx,glue_apd_dy,glue_apd_dz);
        TGeoTranslation *t_glue1 = new TGeoTranslation("t_glue1",-apd_sep_dx-apd_dx,0.,-capsule_dz+glue_apd_dz);
        TGeoTranslation *t_glue2 = new TGeoTranslation("t_glue2",+apd_sep_dx+apd_dx,0.,-capsule_dz+glue_apd_dz);
        apd_glue->SetLineColor(GlueColor);
        t_glue1->RegisterYourself();
        t_glue2->RegisterYourself();
        capsule[iXnode]->AddNode(apd_glue,5,t_glue1);
        capsule[iXnode]->AddNode(apd_glue,6,t_glue2);
        apd_glue->SetVisibility(kTRUE);

// Create a wrapping of 0.5 mm thickness to put crystal in with 10 um of air between crystal and inner surface
// Vertices at -dz : 1 2 and at +dz : 5 6
//                   0 3              4 7
// So, wrapping faces should be parallel to crystal faces
        Double_t sx40=(vertex[4][0]-vertex[0][0])/crystal_dz/2.;
        Double_t sy40=(vertex[4][1]-vertex[0][1])/crystal_dz/2.;
        Double_t sx51=(vertex[5][0]-vertex[1][0])/crystal_dz/2.;
        Double_t sy51=(vertex[5][1]-vertex[1][1])/crystal_dz/2.;
        Double_t sx62=(vertex[6][0]-vertex[2][0])/crystal_dz/2.;
        Double_t sy62=(vertex[6][1]-vertex[2][1])/crystal_dz/2.;
        Double_t sx73=(vertex[7][0]-vertex[3][0])/crystal_dz/2.;
        Double_t sy73=(vertex[7][1]-vertex[3][1])/crystal_dz/2.;
        Double_t wrapping_dxy=0.015;
        Double_t wrapping_dz=0.3;
        coord[0] =vertex[0][0]-wrapping_dxy-sx40*wrapping_dz;
        coord[1] =vertex[0][1]-wrapping_dxy-sy40*wrapping_dz;
        coord[2] =vertex[1][0]-wrapping_dxy-sx51*wrapping_dz;
        coord[3] =vertex[1][1]+wrapping_dxy-sy51*wrapping_dz;
        coord[4] =vertex[2][0]+wrapping_dxy-sx62*wrapping_dz;
        coord[5] =vertex[2][1]+wrapping_dxy-sy62*wrapping_dz;
        coord[6] =vertex[3][0]+wrapping_dxy-sx73*wrapping_dz;
        coord[7] =vertex[3][1]-wrapping_dxy-sy73*wrapping_dz;
        coord[8] =vertex[4][0]-wrapping_dxy+sx40*wrapping_dz;
        coord[9] =vertex[4][1]-wrapping_dxy+sy40*wrapping_dz;
        coord[10]=vertex[5][0]-wrapping_dxy+sx51*wrapping_dz;
        coord[11]=vertex[5][1]+wrapping_dxy+sy51*wrapping_dz;
        coord[12]=vertex[6][0]+wrapping_dxy+sx62*wrapping_dz;
        coord[13]=vertex[6][1]+wrapping_dxy+sy62*wrapping_dz;
        coord[14]=vertex[7][0]+wrapping_dxy+sx73*wrapping_dz;
        coord[15]=vertex[7][1]-wrapping_dxy+sy73*wrapping_dz;
        char nameWF[80], nameWH[80], nameW[80]; // Solid wrapping and Wrappng hole and operation result
        char nameWS[80];
        sprintf(nameWF,"S%2.2d_A%2.2d_WF%2.2d",iStack+1,iAlveola+1,iXtal+1); // solid wrapping (full)
        sprintf(nameWH,"S%2.2d_A%2.2d_WH%2.2d",iStack+1,iAlveola+1,iXtal+1); // wrapping hole
        sprintf(nameWS,"S%2.2d_A%2.2d_WS%2.2d",iStack+1,iAlveola+1,iXtal+1); // composite shape
        sprintf(nameW,"S%2.2d_A%2.2d_W%2.2d",iStack+1,iAlveola+1,iXtal+1);   // final wrapping
        sprintf(nameOP,"%s-%s",nameWF,nameWH);                               // boolean operation
        TGeoArb8 *solid_wrapping = new TGeoArb8(nameWF,crystal_dz+wrapping_dz,coord);
// and make a hole inside :
        wrapping_dxy=0.;
        wrapping_dz=0.25;
        coord[0] =vertex[0][0]-wrapping_dxy-sx40*wrapping_dz;
        coord[1] =vertex[0][1]-wrapping_dxy-sy40*wrapping_dz;
        coord[2] =vertex[1][0]-wrapping_dxy-sx51*wrapping_dz;
        coord[3] =vertex[1][1]+wrapping_dxy-sy51*wrapping_dz;
        coord[4] =vertex[2][0]+wrapping_dxy-sx62*wrapping_dz;
        coord[5] =vertex[2][1]+wrapping_dxy-sy62*wrapping_dz;
        coord[6] =vertex[3][0]+wrapping_dxy-sx73*wrapping_dz;
        coord[7] =vertex[3][1]-wrapping_dxy-sy73*wrapping_dz;
        coord[8] =vertex[4][0]-wrapping_dxy+sx40*wrapping_dz;
        coord[9] =vertex[4][1]-wrapping_dxy+sy40*wrapping_dz;
        coord[10]=vertex[5][0]-wrapping_dxy+sx51*wrapping_dz;
        coord[11]=vertex[5][1]+wrapping_dxy+sy51*wrapping_dz;
        coord[12]=vertex[6][0]+wrapping_dxy+sx62*wrapping_dz;
        coord[13]=vertex[6][1]+wrapping_dxy+sy62*wrapping_dz;
        coord[14]=vertex[7][0]+wrapping_dxy+sx73*wrapping_dz;
        coord[15]=vertex[7][1]-wrapping_dxy+sy73*wrapping_dz;
        TGeoArb8 *wrapping_hole = new TGeoArb8(nameWH,crystal_dz+wrapping_dz,coord);
      // Now build the wrapping
        TGeoCompositeShape *Swrapping = new TGeoCompositeShape(nameWS,nameOP);
        wrapping[iXnode] = new TGeoVolume(nameW,Swrapping,aluminium);
        crystal[iXnode]->AddNode(wrapping[iXnode],2);
        wrapping[iXnode]->SetVisibility(kFALSE);

// Put the porte-ferule in the bottom of the alveola
        Double_t porte_ferule_dz=0.05;
        Double_t hole_radius=0.030;
// First, build a solid porte-ferule :

        char nameFF[80], nameFH[80], nameF[80]; // FUll porte-ferule and porte-ferule hole and operation result
        char nameFS[80], nameB[80], nameFT[80];
        sprintf(nameFF,"S%2.2d_A%2.2d_FF%2.2d",iStack+1,iAlveola+1,iXtal+1); // full porte-ferule
        sprintf(nameFH,"S%2.2d_A%2.2d_FH%2.2d",iStack+1,iAlveola+1,iXtal+1); // porte-ferule hole
        sprintf(nameFS,"S%2.2d_A%2.2d_FS%2.2d",iStack+1,iAlveola+1,iXtal+1); // composite shape
        sprintf(nameF,"S%2.2d_A%2.2d_F%2.2d",iStack+1,iAlveola+1,iXtal+1);   // final porte-ferule
        sprintf(nameB,"S%2.2d_A%2.2d_B%2.2d",iStack+1,iAlveola+1,iXtal+1);   // fiber
        sprintf(nameFT,"S%2.2d_A%2.2d_FT%2.2d",iStack+1,iAlveola+1,iXtal+1);   // fiber
        sprintf(nameOP,"%s-%s",nameFF,nameFH);                               // boolean operation
        TGeoBBox *full_porte_ferule = new TGeoBBox(nameFF,AF/2.,BF/2.,porte_ferule_dz);
// Make hole to put a fiber in :
        TGeoTube *fiber_hole = new TGeoTube(nameFH,0.,hole_radius,porte_ferule_dz+0.0001);
        TGeoCompositeShape *sporte_ferule = new TGeoCompositeShape(nameFS,nameOP);
        porte_ferule[iXnode]= new TGeoVolume(nameF,sporte_ferule,plastic);
        TGeoTranslation *t_porte_ferule;
        //t_porte_ferule = new TGeoTranslation("nameFT",(center[0][0]-center[1][0])/2.,(center[0][1]-center[1][1])/2., -crystal_dz-porte_ferule_dz);
        t_porte_ferule = new TGeoTranslation("nameFT",center[0][0],center[0][1], -crystal_dz-porte_ferule_dz);
        t_porte_ferule->RegisterYourself();
        crystal[iXnode]->AddNode(porte_ferule[iXnode],3,t_porte_ferule);
        porte_ferule[iXnode]->SetVisibility(kTRUE);
// And finally, put a fiber in the hole :
// Make it shorter than the hole length to put air between fiber and crystal
        Double_t fiber_radius=0.015;
        fiber[iXnode] = geom->MakeTube(nameB,fiber_core,0.,fiber_radius,porte_ferule_dz/2.);
        crystal[iXnode]->AddNode(fiber[iXnode],4,t_porte_ferule);
        fiber[iXnode]->SetVisibility(kTRUE);

        iXnode++;
      }
      iAnode++;
    }
  }
// Add equivalent MCP in front of ECAL
  MCP = geom->MakeBox("MCP",MCP_med,100.,100.,0.3);
  TGeoTranslation *t_mcp = new TGeoTranslation("T_MCP",0.,0.,-50.);

  TH2D *h2Cx=new TH2D("h2Cx","h2Cx",100,-0.5,2.5,920,-11.5,11.5);
  h2Cx->SetLineColor(kBlue);
  h2Cx->SetMarkerColor(kBlue);
  TH2D *h2Cy=new TH2D("h2Cy","h2Cy",100,-0.5,2.5,920,-11.5,11.5);
  h2Cy->SetLineColor(kBlue);
  h2Cy->SetMarkerColor(kBlue);
  TH3D *h3C=new TH3D("h3C","h3C",100,-0.5,2.5,100,-0.5,2.5,920,-11.5,11.5);
  h3C->SetLineColor(kBlue);
  h3C->SetMarkerColor(kBlue);
  TH2D *h2Ex=new TH2D("h2Ex","h2Ex",100,-0.5,2.5,920,-11.5,11.5);
  h2Ex->SetLineColor(kRed);
  h2Ex->SetMarkerColor(kRed);
  TH2D *h2Ey=new TH2D("h2Ey","h2Ey",100,-0.5,2.5,920,-11.5,11.5);
  h2Ey->SetLineColor(kRed);
  h2Ey->SetMarkerColor(kRed);
  TH3D *h3E=new TH3D("h3E","h3E",100,-0.5,2.5,100,-0.5,2.5,920,-11.5,11.5);
  h3E->SetLineColor(kRed);
  h3E->SetMarkerColor(kRed);
  typedef struct { Int_t iXtal, iType; Float_t t, x, y, z, Kx, Ky, Kz, E; } hit_data_t;
  // iXtal/I:iType/I:t/F:posX/F:posY/F:posZ/F:dirX/F:dirY/F:dirZ/F:E/F
  hit_data_t HitData;
  Double_t *pts;
  pts=(Double_t *)malloc(100000000*sizeof(Double_t));
  TEvePointSet* psE = new TEvePointSet();
  psE->SetOwnIds(kTRUE);
  psE->SetMarkerStyle(20);
  psE->SetMarkerSize(0.1);
  //TEvePointSet *psC;

  if(evt_file!="")
  {
    TFile *fd=new TFile(evt_file);
    TTree *tr=(TTree*)fd->Get("G4_tree");
    char Bname[80];
    sprintf(Bname,"Hits_%4.4d",ievt);
    TBranch *fB=tr->GetBranch(Bname);
    tr->SetBranchAddress(Bname,&HitData);
    Int_t nEntries=fB->GetEntries();
    tr->SetEntries(nEntries);
    printf("%d hits found in file\n",nEntries);
    Double_t E_tot=0., E_seen=0.;
    Int_t Ndep=0;
    for(Int_t i=0; i<nEntries; i++)
    {
      tr->GetEntry(i);
      Double_t x=HitData.x/10.; // Convert to cm
      Double_t y=HitData.y/10.;
      Double_t z=HitData.z/10.;
      Double_t t=HitData.t;
      Double_t Kx=HitData.Kx;
      Double_t Ky=HitData.Ky;
      Double_t Kz=HitData.Kz;

      if(HitData.iType==0) // dEdx
      {
        TGeoNode *loc_node=gGeoManager->InitTrack(x,y,z,Kx,Ky,Kz);
        if(!loc_node)
        {
          if(i<1000)printf("Current point %.1f %.1f %.1f outside detector\n",x,y,z);
        }
        else
        {
          TGeoVolume *loc_vol=loc_node->GetVolume();

          TGeoVolume *Edep = geom->MakeBox("Edep",vacuum,0.005,0.005,0.005);
          Edep->SetVisibility(kTRUE);
          Edep->SetTransparency(0);
          Edep->SetLineColor(EdepColor);
          Edep->SetLineWidth(1);
          if(i<1000)printf("Current point %.1f %.1f %.1f inside detector %s, %s\n",x,y,z, loc_node->GetName(), loc_vol->GetName());
          if(loc_vol->GetName()==targetX || loc_vol->GetName()==targetXS) E_seen+=HitData.E;
        }
        E_tot+=HitData.E;
        if((Ndep%10000)==0)
        {
          h3E->Fill(x,y,z,1.);
          h2Ex->Fill(x,z,1.);
          h2Ey->Fill(y,z,1.);
          //psE->SetPointId(new TNamed(Form("PointC %d", Ndep/10000), ""));
        }
        psE->SetNextPoint(x,y,z);
        pts[Ndep*3]=x;
        pts[Ndep*3+1]=y;
        pts[Ndep*3+2]=z;
        Ndep++;
      }
    }
    printf("Total shower energy : %.2f GeV, seen : %.2f GeV\n",E_tot/1000.,E_seen/1000.);
    fd->Close();
  }
  geom->CloseGeometry();
  //geom->CheckOverlaps(0.01);
  geom->GetListOfNodes();
  TGeoVolume *tv=geom->GetVolume("S01_A02_X03");
  printf("target Xtal %s\n",tv->GetName());

  top->SetVisibility(kFALSE);
  top->SetVisContainers();
  top->SetLineColor(1);
  top->SetLineWidth(1);
  gGeoManager->SetTopVisible(1);
  gGeoManager->SetVisLevel(5);

  TEveManager::Create();
  TEveGeoTopNode* tn = new TEveGeoTopNode(gGeoManager, gGeoManager->GetTopNode());
  tn->SetVisLevel(4);
  psE->SetMarkerColor(kBlue);
  gEve->AddGlobalElement(psE);
  gEve->AddGlobalElement(tn);
  gEve->FullRedraw3D(kTRUE);
  //gEve->Redraw3D(kTRUE);

  //top->Draw("ogl");
  //h3E->Draw("");
  //TPolyMarker3D *tp3D=new TPolyMarker3D(1000000,pts,20);
  //tp3D->Draw();

  //for(int ips=0; ips<100;ips++)
  //{
  //  psC[ips] = new TEvePointSet();
  //  psC[ips]->SetOwnIds(kTRUE);
  //}
  //
  //for(Int_t ips=0; ips<100; ips++)
  //{
  //  gEve->AddGlobalElement(psC[ips]);
  //}

  // EClipType not exported to CINT (see TGLUtil.h):
  // 0 - no clip, 1 - clip plane, 2 - clip box
  /*
  TGLViewer *v = gEve->GetDefaultGLViewer();
  v->GetClipSet()->SetClipType(TGLClip::EType(1));
  v->ColorSet().Background().SetColor(kMagenta+4);
  v->SetGuideState(TGLUtil::kAxesEdge, kTRUE, kFALSE, 0);
  v->RefreshPadEditor(v);

  v->CurrentCamera().RotateRad(-1.2, 0.5);
  v->DoDraw();
  */
}
