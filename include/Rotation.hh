#ifndef Rotation_h
#define Rotation_h 1


#include "globals.hh"


class Rotation 
{
  public:
    Rotation();
    ~Rotation();

  private:
    double XX, XY, XZ, YX, YY, YZ, ZX, ZY, ZZ;
     
     
  public:
    void position_rot(double,double,double,double,double,double); 
    double  XXr();
    double  XYr();
    double  XZr();
    double  YXr();
    double  YYr();
    double  YZr();
    double  ZXr();
    double  ZYr();
    double  ZZr();
};
#endif
