#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4ParticleGun.hh"

class G4Event;
class G4UIcmdWithAString;
class DetectorConstruction;
class PrimaryGeneratorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction 
{
public:
  PrimaryGeneratorAction(DetectorConstruction*) ;

  ~PrimaryGeneratorAction();

public:
  void GeneratePrimaries(G4Event*);
 
  //void SetGunParticle(G4String val) { gunParticle = val;};
  void SetRndmSeed(G4int val) { rndmSeed = val;};
  //  void SetBalai(G4String val) { balai = val;};
  void SetBeamType(G4String val) { beamType = val;};
  void SetBeamProfile(G4String val) { beamProfile = val;};
  void SetposXY(G4String val);
  void SetposEtaPhi(G4String val);
  void SetEnergy(G4double ene){ particleGun->SetParticleEnergy(ene); };
  void SetXHodo(G4double xhodo){ xHodo=xhodo; };
  void SetYHodo(G4double yhodo){ yHodo=yhodo; };
  void SetBeamXMin(G4double beamx){ beam_x_min=beamx;};
  void SetBeamXMax(G4double beamx){ beam_x_max=beamx;};
  void SetBeamXMean(G4double beamx){ beam_x_mean=beamx; };
  void SetBeamXSigma(G4double beamx){ beam_x_sigma=beamx; };
  void SetBeamYMin(G4double beamy){ beam_y_min=beamy; };
  void SetBeamYMax(G4double beamy){ beam_y_max=beamy; };
  void SetBeamYMean(G4double beamy){ beam_y_mean=beamy; };
  void SetBeamYSigma(G4double beamy){ beam_y_sigma=beamy; };
  void SetDTheta(G4double dtheta){ d_theta=dtheta; };
  void SetDPhi(G4double dphi){ d_phi=dphi; };
  void SetDeltaX(G4double dx){ delta_x=dx; };
  void SetDeltaY(G4double dy){ delta_y=dy; };

  G4int GetRndmSeed(){ return rndmSeed; };
  G4int GetposX(){ return pos_X_init; };
  G4int GetposY(){ return pos_Y_init; };
  G4int GetposPhi(){ return pos_phi_init; };
  G4int GetposEta(){ return pos_eta_init; };
  G4double GetEnergy(){ return particleGun->GetParticleEnergy(); };
  G4double GetXHodo(){ return xHodo; };
  G4double GetYHodo(){ return yHodo; };
  G4double GetBeamXMin(){ return beam_x_min; };
  G4double GetBeamXMax(){ return beam_x_max; };
  G4double GetBeamXMean(){ return beam_x_mean; };
  G4double GetBeamXSigma(){ return beam_x_sigma; };
  G4double GetBeamYMin(){ return beam_y_min; };
  G4double GetBeamYMax(){ return beam_y_max; };
  G4double GetBeamYMean(){ return beam_y_mean; };
  G4double GetBeamYSigma(){ return beam_y_sigma; };
  G4double GetDTheta(){ return d_theta; };
  G4double GetDPhi(){ return d_phi; };
  G4double GetDeltaX(){ return delta_x; };
  G4double GetDeltaY(){ return delta_y; };


private:
  //Methods used to construct the position of the maximun of response position as in H4

  G4double GetTabEta(G4int val) { if(val>=0) return tab_eta[val] ; else return 90.0l;}
  G4double GetTabPhi(G4int val) { if(val>=0) return tab_phi[val] ; else return 0.0l;}

  G4RotationMatrix ConversionMatrix(G4double,G4double,G4double);

  G4ParticleGun*          particleGun;	    //pointer a to G4  class
  DetectorConstruction*   Detector;  //pointer to the geometry
  
  FILE* 			  ptrInit_intermediaire;
  
  PrimaryGeneratorMessenger* gunMessenger; //messenger of this class
  G4String      gunParticle;	             //Type of particle to be generated
  G4String      rndmFlag;	                 //flag for a rndm impact point
  G4String      balai;	 
  G4String      beamType;    //Center,TB,Custom(not yet implemented)
  G4String      beamProfile; //Flat,Gaussian
  char geometrypath[150] ;
  char center_file[200] ;

  G4ThreeVector center_position;
  G4double xHodo;
  G4double yHodo;
  G4int rndmSeed;
  G4int pos_X_init;
  G4int pos_Y_init;
  G4int pos_eta_init;
  G4int pos_phi_init;
  G4int compteur_eta;
  G4double ref_eta;
  G4double beam_x_min;
  G4double beam_x_max;
  G4double beam_x_mean;
  G4double beam_x_sigma;
  G4double beam_y_min;
  G4double beam_y_max;
  G4double beam_y_mean;
  G4double beam_y_sigma;
  G4double d_theta;
  G4double d_phi;
  G4double delta_x;
  G4double delta_y;
  G4double tab_eta[85] ;
  G4double tab_phi[20] ;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


