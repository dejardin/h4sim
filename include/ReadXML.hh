#ifndef ReadXML_h
#define ReadXML_h 1


#include "globals.hh"

class ReadXML
{
   public:
   
     ReadXML(char*);
     ~ReadXML();
     
   public:
     
     void XtalFile(FILE *Geometry);
     void DirectionFile(FILE *Direction);
     void RotationFile(char *rot);
     void AlveolaFile(FILE *Direction);
     void searchRotation(char *word,FILE *tempo,char* texte);
     double units(double value,FILE *tempo);
     void search(char *word,FILE *tempo, int output,char *text);
     double search( char *word,FILE *tempo, int output);
     std::string XtalName();

     double parm0();
     double parm1();
     double parm2();
     double parm3();
     double parm4();
     double parm5();
     double parm6();
     double parm7();
     double parm8();
     double parm9();
     double parm10();
     double translate0();
     double translate1();
     double translate2();
     double translateAl0();
     double translateAl1();
     double translateAl2();
     double angles0();
     double angles1();
     double angles2();
     double angles3();
     double angles4();
     double angles5();
   
   private:
   
  char geometrypath[150];
     int counter;
     char loc_char[64];
     char passage[64];
     char Xtal_name[64];
     
     
     FILE *Directtion;
     FILE *Direction2;
     FILE *Rotation;
     
     double parameter[11];
     char find[64];
     int nchar;
     double translate[3];
     double translateAl[3];
     double angles[6];
     char rotate[64];
     char rotateAl[64];
};


     
     




#endif
