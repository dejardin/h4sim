

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef EcalHit_h
#define EcalHit_h 1

#include "DetectorConstruction.hh"
#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EcalHit : public G4VHit
{
 public:

   EcalHit();
  ~EcalHit();
   EcalHit(const EcalHit&);
   const EcalHit& operator=(const EcalHit&);
   int operator==(const EcalHit&) const;

   inline void* operator new(size_t);
   inline void  operator delete(void*);

   void Draw();
   void Print();
      
 public:
  
   void SetHit(G4int VolId, G4int type, G4float time, G4ThreeVector pos, G4ThreeVector dir, G4float E)
   {
     fXtalId=VolId;
     fType=type;
     fTime=time;
     fPos=pos;
     fDir=dir;
     fE=E;
     //printf("Set new hit : %d %d %e\n",fXtalId, fType,fE);
   }
                 
   G4int         GetXtalId() { return fXtalId; };
   G4int         GetType()   { return fType; };
   G4ThreeVector GetPos()    { return fPos; };
   G4ThreeVector GetDir()    { return fDir; };
   G4float       GetTime()   { return fTime; };
   G4float       GetE()      { return fE; };
    
 private:
  
   G4int         fXtalId; // ECAL numbering
   G4int         fType;   // 0=dE/dx, 1=Cerenkov
   G4ThreeVector fPos, fDir;
   G4float       fTime;   // Elapsed time since beginning of event
   G4float       fE;      // Deposited energy or photon energy
        
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<EcalHit> EcalHitsCollection;

extern G4Allocator<EcalHit> EcalHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* EcalHit::operator new(size_t)
{
  void* aHit;
  aHit = (void*) EcalHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void EcalHit::operator delete(void* aHit)
{
  EcalHitAllocator.FreeSingle((EcalHit*) aHit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


