
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#ifndef CalorimeterSD_h
#define CalorimeterSD_h 1

#include "G4VSensitiveDetector.hh"
#include "globals.hh"

class DetectorConstruction;
class G4HCofThisEvent;
class G4Step;
#include "EcalHit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class CalorimeterSD : public G4VSensitiveDetector
{
  public:
  
      CalorimeterSD(G4String,DetectorConstruction* det);
     ~CalorimeterSD();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*,G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);
      void clear();
      void DrawAll();
      void PrintAll();

  private:
  
      EcalHitsCollection*   fEcalCollection;      
      DetectorConstruction* fDetector;
      G4int*                fHitID;
};

#endif

