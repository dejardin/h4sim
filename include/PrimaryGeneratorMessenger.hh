
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef PrimaryGeneratorMessenger_h
#define PrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class PrimaryGeneratorAction;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PrimaryGeneratorMessenger: public G4UImessenger
{
  public:
    PrimaryGeneratorMessenger(PrimaryGeneratorAction*);
   ~PrimaryGeneratorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  PrimaryGeneratorMessenger *gunMessenger; //messenger of this class
  //G4UIcmdWithAString        *gunParticle;

  PrimaryGeneratorAction    *Action; 
  G4UIcmdWithAString        *beamtype;
  G4UIcmdWithAString        *beamprofile;
  G4UIcmdWithAString        *posXY;
  G4UIcmdWithAString        *posEtaPhi;

  G4UIcmdWithADoubleAndUnit *beamxmin;
  G4UIcmdWithADoubleAndUnit *beamxmax;
  G4UIcmdWithADoubleAndUnit *beamxmean;
  G4UIcmdWithADoubleAndUnit *beamxsigma;
  G4UIcmdWithADoubleAndUnit *beamymin;
  G4UIcmdWithADoubleAndUnit *beamymax;
  G4UIcmdWithADoubleAndUnit *beamymean;
  G4UIcmdWithADoubleAndUnit *beamysigma;

  G4UIcmdWithADoubleAndUnit *xoff;
  G4UIcmdWithADoubleAndUnit *yoff;
  G4UIcmdWithADouble        *dtheta;
  G4UIcmdWithADouble        *dphi;
  G4UIcmdWithADoubleAndUnit *dx;
  G4UIcmdWithADoubleAndUnit *dy; 
  G4UIcmdWithAnInteger      *RndmCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

