#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"

class EventActionMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EventAction : public G4UserEventAction
{
  public:
    EventAction(TTree *DataTree, DetectorConstruction *detector, PrimaryGeneratorAction* mpg);
    virtual ~EventAction();

  public:
    virtual void   BeginOfEventAction(const G4Event*);
    virtual void   EndOfEventAction(const G4Event*);
    
    void SetDrawFlag       (G4String val)  {fdrawFlag = val;};
    void SetCerenkovFlag   (G4String val)  {fCerenkovFlag = val;};
    void SetdEdxFlag       (G4String val)  {fdEdxFlag = val;};
    void SetPrintModulo    (G4int    val)  {fprintModulo = val;};
    void SetPrintOn        (G4int    val)  {fPrintOn = val;};
 
  private:
    G4int                         fPrintOn;
    G4int                         fEcalColId;
    G4String                      fdrawFlag;
    G4String                      fCerenkovFlag;
    G4String                      fdEdxFlag;
    G4int                         fprintModulo;                         
    EventActionMessenger*   feventMessenger;
    PrimaryGeneratorAction* fprimaryGeneratorAction;
    DetectorConstruction*   fDetector;
    TTree*		              fTree;
    TBranch*                fB_MC;
    TBranch*                fB_Xtal;
    TBranch*                fB_Hits;
    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
