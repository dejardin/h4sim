
#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1


#include "G4VUserDetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"

#include <vector>
 
const int max_nb_alveola=17;
#ifndef TB_2018
const int             nb_Xtal = 1700;
const int             nb_alveola = 17;
const int             alveola_type[nb_alveola]= { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16};
const int             alveola_stack=10;
#else
const int             nb_Xtal = 30;
const int             nb_alveola = 3;
const int             alveola_type[nb_alveola] ={7,5,6};
const int             alveola_stack=1;
#endif

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;
class DetectorMessenger;
class CalorimeterSD;
class G4AssemblyVolume;
class G4Trap;
class G4Tubs;
class G4Cons;


class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
   ~DetectorConstruction();

  public:
    void SetVisSM(G4int);
    void SetVisAlu(G4String);
    void SetModAngles(G4String);
       
    G4VPhysicalVolume* Construct();

    void UpdateGeometry();
     
  public:
    G4int visu;
    G4int VisAlu;
    G4double PosX, PosY, ModTheta, ModPhi;
     
    FILE *Geometry;
    FILE *Alveola;
    FILE *Direction;          
     
    const G4VPhysicalVolume* GetXtal(int Xtal_number) {return physicalCrystal[Xtal_number];};
      
   private:                 
     char geometrypath[150];   
     G4Material        *WorldMaterial;
     G4Material        *AlveolaMaterial;
     G4Material        *CrystalMaterial;
     G4Material        *defaultMaterial;
     G4Material        *FrameMaterial;

     const G4double     PosX_ref=0.0*mm;
     const G4double     PosY_ref=0.0*mm;
     const G4double     ModTheta_ref=21.7*CLHEP::deg;
     const G4double     ModPhi_ref=-1.*CLHEP::deg;
     G4double           WorldSizeYZ;
     G4double           WorldSizeX;
  
     G4Box             *solidWorld;      //pointer to the solid World 
     G4LogicalVolume   *logicalWorld;      //pointer to the logical World
     G4VPhysicalVolume *physicalWorld;      //pointer to the physicalWorld
  
     G4Trap            *solidAlveola[nb_alveola*alveola_stack];   //pointer to the solid Alveola
     G4LogicalVolume   *logicalAlveola[nb_alveola*alveola_stack];   //pointer to the logical Alveola
     G4VPhysicalVolume *physicalAlveola[nb_alveola*alveola_stack];   //pointer to the physical Alveola
  
     G4Trap             *solidCrystal[nb_Xtal];
     G4LogicalVolume    *logicalCrystal[nb_Xtal];      //pointer to the logical Cristal
     G4VPhysicalVolume  *physicalCrystal[nb_Xtal];      //pointer to the physical Cristal
  
     G4Box             *solidPlateInfCD;      
     G4LogicalVolume   *logicalPlateInfCD;      
     G4VPhysicalVolume *physicalPlateInfCD;      
  
     G4Box             *solidPlateInfCG;      
     G4LogicalVolume   *logicalPlateInfCG;      
     G4VPhysicalVolume *physicalPlateInfCG;
  
     G4Tubs             *solidPlateInfP;      
     G4LogicalVolume    *logicalPlateInfP;      
     G4VPhysicalVolume  *physicalPlateInfP;
  
     G4Tubs             *solidPlateInfP2;      
     G4LogicalVolume    *logicalPlateInfP2;      
     G4VPhysicalVolume  *physicalPlateInfP2;

     G4Tubs             *solidPlateInfP3;      
     G4LogicalVolume    *logicalPlateInfP3;      
     G4VPhysicalVolume  *physicalPlateInfP3;
  
     G4Box             *solidE1CD;      
     G4LogicalVolume   *logicalE1CD;      
     G4VPhysicalVolume *physicalE1CD;      
  
     G4Box             *solidE1CG;      
     G4LogicalVolume   *logicalE1CG;      
     G4VPhysicalVolume *physicalE1CG;
  
     G4Cons             *solidE1CN;      
     G4LogicalVolume    *logicalE1CN;      
     G4VPhysicalVolume  *physicalE1CN;

     G4Tubs             *solidE1EC;      
     G4LogicalVolume    *logicalE1EC;      
     G4VPhysicalVolume  *physicalE1EC;

     G4Box             *solidE2CD;      
     G4LogicalVolume   *logicalE2CD;      
     G4VPhysicalVolume *physicalE2CD;      

     G4Box             *solidE2CG;      
     G4LogicalVolume   *logicalE2CG;      
     G4VPhysicalVolume *physicalE2CG;

     G4Cons             *solidE2CN;      
     G4LogicalVolume    *logicalE2CN;      
     G4VPhysicalVolume  *physicalE2CN;

     G4Tubs             *solidE2EC;      
     G4LogicalVolume    *logicalE2EC;      
     G4VPhysicalVolume  *physicalE2EC;

     G4Box             *solidE3CD;      
     G4LogicalVolume   *logicalE3CD;      
     G4VPhysicalVolume *physicalE3CD;      

     G4Box             *solidE3CG;      
     G4LogicalVolume   *logicalE3CG;      
     G4VPhysicalVolume *physicalE3CG;

     G4Cons             *solidE3CN;      
     G4LogicalVolume    *logicalE3CN;      
     G4VPhysicalVolume  *physicalE3CN;

     G4Tubs             *solidE3EC;      
     G4LogicalVolume    *logicalE3EC;      
     G4VPhysicalVolume  *physicalE3EC;

     G4Box             *solidE4CD;      
     G4LogicalVolume   *logicalE4CD;      
     G4VPhysicalVolume *physicalE4CD;      

     G4Box             *solidE4CG;      
     G4LogicalVolume   *logicalE4CG;      
     G4VPhysicalVolume *physicalE4CG;

     G4Cons             *solidE4CN;      
     G4LogicalVolume    *logicalE4CN;      
     G4VPhysicalVolume  *physicalE4CN;

     G4Tubs             *solidE4EC;      
     G4LogicalVolume    *logicalE4EC;      
     G4VPhysicalVolume  *physicalE4EC;
       
     G4Box             *solidE5CD;      
     G4LogicalVolume   *logicalE5CD;      
     G4VPhysicalVolume *physicalE5CD;      

     G4Box             *solidE5CG;      
     G4LogicalVolume   *logicalE5CG;      
     G4VPhysicalVolume *physicalE5CG;

     G4Box             *solidE6CD;      
     G4LogicalVolume   *logicalE6CD;      
     G4VPhysicalVolume *physicalE6CD;      

     G4Box             *solidE6CG;      
     G4LogicalVolume   *logicalE6CG;      
     G4VPhysicalVolume *physicalE6CG;

     G4Box             *solidE7CD;      
     G4LogicalVolume   *logicalE7CD;      
     G4VPhysicalVolume *physicalE7CD;      

     G4Box             *solidE7CG;      
     G4LogicalVolume   *logicalE7CG;      
     G4VPhysicalVolume *physicalE7CG;

     G4Trap            *solidE8CD;      
     G4LogicalVolume   *logicalE8CD;      
     G4VPhysicalVolume *physicalE8CD;      

     G4Trap            *solidE8CG;      
     G4LogicalVolume   *logicalE8CG;      
     G4VPhysicalVolume *physicalE8CG;

     DetectorMessenger* detectorMessenger;  //pointer to the Messenger
     CalorimeterSD* calorimeterSD=NULL;  //pointer to the sensitive detector

    //std::vector<G4VPhysicalVolume*> vect;
        
  private:
    void DefineMaterials();
    G4VPhysicalVolume* ConstructCalorimeter();     
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif
