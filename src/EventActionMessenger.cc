
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "EventActionMessenger.hh"

#include "EventAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventActionMessenger::EventActionMessenger(EventAction* EvAct)
:eventAction(EvAct)
{ 
  DrawCmd = new G4UIcmdWithAString("/event/drawTracks",this);
  DrawCmd->SetGuidance("Draw the tracks in the event");
  DrawCmd->SetGuidance("  Choice : none, charged(default),neutral, all");
  DrawCmd->SetParameterName("choice",true);
  DrawCmd->SetDefaultValue("charged");
  DrawCmd->SetCandidates("none charged neutral all");
  DrawCmd->AvailableForStates(G4State_Idle);
  
  SaveCerenkovCmd = new G4UIcmdWithAString("/event/saveCerenkov",this);
  SaveCerenkovCmd->SetGuidance("Save Cerenkov photons parameters produced in the event");
  SaveCerenkovCmd->SetGuidance("  Choice : false, true(default)");
  SaveCerenkovCmd->SetParameterName("choice",true);
  SaveCerenkovCmd->SetDefaultValue("true");
  SaveCerenkovCmd->SetCandidates("true false");
  SaveCerenkovCmd->AvailableForStates(G4State_Idle);
  
  SavedEdxCmd = new G4UIcmdWithAString("/event/savedEdx",this);
  SavedEdxCmd->SetGuidance("Save dEdx parameters produced in the event");
  SavedEdxCmd->SetGuidance("  Choice : false, true(default)");
  SavedEdxCmd->SetParameterName("choice",true);
  SavedEdxCmd->SetDefaultValue("true");
  SavedEdxCmd->SetCandidates("true false");
  SavedEdxCmd->AvailableForStates(G4State_Idle);
  
  PrintCmd = new G4UIcmdWithAnInteger("/event/printModulo",this);
  PrintCmd->SetGuidance("Print events modulo n");
  PrintCmd->SetParameterName("EventNb",false);
  PrintCmd->SetRange("EventNb>0");
  PrintCmd->AvailableForStates(G4State_Idle);     
  
  PrintOnCmd = new G4UIcmdWithAnInteger("/event/printOn",this);
  PrintOnCmd->SetGuidance("Print debug information");
  PrintOnCmd->SetParameterName("printOn",false);
  PrintOnCmd->SetDefaultValue(0);
  PrintOnCmd->AvailableForStates(G4State_Idle);     
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventActionMessenger::~EventActionMessenger()
{
  delete DrawCmd;
  delete SaveCerenkovCmd;
  delete SavedEdxCmd;
  delete PrintCmd;
  delete PrintOnCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventActionMessenger::SetNewValue( G4UIcommand* command,G4String newValue)
{ 
  if(command == DrawCmd)         {eventAction->SetDrawFlag(newValue);}
  if(command == SaveCerenkovCmd) {eventAction->SetCerenkovFlag(newValue);}
  if(command == SavedEdxCmd)     {eventAction->SetdEdxFlag(newValue);}
  if(command == PrintCmd)        {eventAction->SetPrintModulo(PrintCmd->GetNewIntValue(newValue));}
  if(command == PrintOnCmd)      {eventAction->SetPrintOn(PrintOnCmd->GetNewIntValue(newValue));}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
