#include "EventAction.hh"

#include "EcalHit.hh"
#include "EventActionMessenger.hh"

#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"

using namespace std;
using namespace CLHEP;
typedef struct { Float_t energy; Int_t Xtal_target; Float_t d_eta,d_phi,xHodo,yHodo,xDet,yDet; } gen_data_t;
typedef struct { Int_t iXtal, iType; Float_t t, x, y, z, dx, dy, dz, E; } hit_data_t;
Float_t Xtal_data[nb_Xtal];
hit_data_t hit_data; // numeve, Xtal_number, Type, Time, x, y, z, dx, dy, dz, E
gen_data_t gen_data;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction(TTree *DataTree, DetectorConstruction *mdc, PrimaryGeneratorAction* mpg)
{
  fPrintOn = 0;
  fprintModulo = 1;
  fEcalColId = -1;
  fdrawFlag = "all";
  feventMessenger = new EventActionMessenger(this);
  fprimaryGeneratorAction = mpg;
  fDetector = mdc;
  fTree=DataTree;
  fB_MC=fTree->Branch( "MC_infos", &gen_data, "Energy/F:Xtal_target/I:d_eta/F:d_phi/F:xHodo/F:yHodo/F:xDet/F:yDet/F");
  char bname[80];
  sprintf(bname,"Edep[%d]/F",nb_Xtal);
  fB_Xtal=fTree->Branch("Crystals",Xtal_data,bname);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{
  delete feventMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event* evt)
{
  
  G4int evtNb = evt->GetEventID();
  if (evtNb%fprintModulo == 0)
  { 
    G4cout << "\n---> Begin of event: " << evtNb << G4endl;
    CLHEP::HepRandom::showEngineStatus();
  }
    
  if (fEcalColId==-1)
  {
    G4SDManager * SDman = G4SDManager::GetSDMpointer();
    fEcalColId = SDman->GetCollectionID("EcalCollection");
  } 
  char bname[80];
  G4int seed = fprimaryGeneratorAction->GetRndmSeed();
  sprintf(bname,"Hits_%4.4d",(seed-1)*100+evtNb);
  //sprintf(bname,"Hits_%4.4d",(seed-1)*50+evtNb);
  if(fdEdxFlag=="true" || fCerenkovFlag=="true")
    fB_Hits=fTree->Branch(bname,&hit_data,"iXtal/I:iType/I:t/F:posX/F:posY/F:posZ/F:dirX/F:dirY/F:dirZ/F:E/F");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* evt)
{
#ifdef TB_2018
// Convert G4 numbering to TB electronic channel mapping
// We start with the bottom row of each alveola, then the top one
// And we stack alveola (7-5-6)
// Top row is not equipped with electronics [25 .. 29] (so, line 2 above)
// The VFE boards read the channels vertically in snake numbering
  G4int TB_index[nb_Xtal]={
                           20, 19, 10,  9,  0,
                           29, 28, 27, 26, 25,
                           22, 17, 12,  7,  2,
                           21, 18, 11,  8,  1,
                           24, 15, 14,  5,  4,
                           23, 16, 13,  6,  3};
// View with beam in back to make relation between (posX-posY) to electronic numbering :
  G4int TB_pos[nb_Xtal]=  {29, 28, 27, 26, 25,
                           20, 19, 10,  9,  0,
                           21, 18, 11,  8,  1,
                           22, 17, 12,  7,  2,
                           23, 16, 13,  6,  3,
                           24, 15, 14,  5,  4};
#endif

  G4int evtNb = evt->GetEventID();
  G4double Xtal_energy[nb_Xtal]={0.};    //Energy deposited in each crystal
  G4int    Xtal_nCerenkov[nb_Xtal]={0};  //Number of Cerenkov Photons in each crystal
  G4int nCerenkov=0;
  G4int PGC = 0;
  G4int y2 = 0;
  G4int y3 = 0;
  G4int bloc , rep;
  G4int cristal_eta;
  G4int cristal_phi;
  
// Retreive energy of hits and compute deposited energy in crystals
  G4HCofThisEvent* EventHC = evt->GetHCofThisEvent();
  EcalHitsCollection* EcalHC = 0;
  G4int n_hit = 0;
  G4double totEdep=0.;
  
//=======================================================================
//----------------Total deposited energy---------------
  if (EventHC) EcalHC = (EcalHitsCollection*)(EventHC->GetHC(fEcalColId));

  if (EcalHC)
  {
    n_hit = EcalHC->entries();
    if(fPrintOn>0)printf("Number of hits in the collection : %d\n",n_hit);
    for (G4int i=0;i<n_hit;i++)
    {
      //printf("Hit %d : Xtal %d, type %d, E=%e\n",i,(*EcalHC)[i]->GetXtalId(),(*EcalHC)[i]->GetType(),(*EcalHC)[i]->GetE());
      G4int ixtal=(*EcalHC)[i]->GetXtalId();
      if((*EcalHC)[i]->GetType()==0)
      {
        G4double Edep=(*EcalHC)[i]->GetE();
        totEdep += Edep; 
        Xtal_energy[ixtal] += Edep ;
      }
      if((*EcalHC)[i]->GetType()==1)
      {
        Xtal_nCerenkov[ixtal]++;
        nCerenkov++;
      }
    }
  }
  if(fPrintOn>0)
  {
    for (G4int ixtal=0; ixtal<nb_Xtal; ixtal++)
    {
      printf("VolId %d : E=%7.3f MeV, %d Cerenkov photons\n",ixtal,Xtal_energy[ixtal], Xtal_nCerenkov[ixtal]);
    }
  }
   
//==================================================================
//----------------- Drawing events------------------------
  G4cout << "---> End of event: " << evtNb << G4endl;  
  G4cout << "   Crystals total energy: " << setw(7)
         << G4BestUnit(totEdep,"Energy") << "; Total number of cerenkov photons: " << nCerenkov
         << G4endl;
    
//==================================================================
//---Convert G4 numbering to SM numbering-----
  for(y3 = 0 ; y3 < nb_Xtal ; y3++)
  {
    PGC = y3;
    bloc = PGC / 100;
    rep = PGC - bloc * 100;
   
#ifdef TB_2018
    y2=TB_index[y3];
#else
    if(rep < 50 )
    {
      cristal_phi =19 - (2 * (1 + rep % 10)-1);
      cristal_eta = rep / 10 + 5 * bloc; 
    }
    else
    {
      cristal_phi = 19 - 2* (rep % 10) ;
      cristal_eta = (rep - 50) / 10 + 5 * bloc;
    }
    y2 = (cristal_eta) + 85 * (cristal_phi);
#endif
    Xtal_data[y2] = (G4float)Xtal_energy[y3];
  }

#ifdef TB_2018
  if(fPrintOn>0)
  {
    for(int irow=0; irow<6; irow++)
    {
      for(G4int icol=0; icol<5; icol++)
      {
        G4int i=TB_pos[irow*5+icol];
        printf("%7.3f ",Xtal_data[i]/GeV);
      }
      printf("\n");
    }
  }
#endif
  
  int eta_i,phi_i,Xtal_number;
  G4double E_i,d_eta_i,d_phi_i,xHodo_i,yHodo_i; 
  E_i     = fprimaryGeneratorAction->GetEnergy();
  eta_i   = fprimaryGeneratorAction->GetposEta();
  phi_i   = fprimaryGeneratorAction->GetposPhi();
  xHodo_i = fprimaryGeneratorAction->GetXHodo();
  yHodo_i = fprimaryGeneratorAction->GetYHodo();
  d_eta_i = fprimaryGeneratorAction->GetDTheta();
  d_phi_i = fprimaryGeneratorAction->GetDPhi();
  
  cout << "This particle has E: " << E_i << " Eta : " <<  eta_i << " Phi: " << phi_i << " Xoff : " << xHodo_i << " Yoff: " << yHodo_i << endl;

#ifdef TB_2018
// Get the target crystal from the table position :
  G4double PosX=fDetector->PosX;
  G4double PosY=fDetector->PosY;
  G4int iCol=PosX/(2.5*cm);
  if(iCol<0)iCol=0;
  if(iCol>4)iCol=4;
  G4int iRow=PosY/(2.4*cm);
  if(iRow<0)iRow=0;
  if(iRow>5)iRow=5;
  Xtal_number=TB_pos[iRow*5+iCol];
#else
  Xtal_number = (eta_i) + 85 * (phi_i) ;
#endif
  
  if(fPrintOn>0)G4cout <<  E_i << Xtal_number << eta_i << phi_i << d_eta_i << d_phi_i << G4endl;  
  
  gen_data.energy = (G4float)E_i ;
  gen_data.Xtal_target = Xtal_number;
  gen_data.d_eta=(G4float)d_eta_i;
  gen_data.d_phi=(G4float)d_phi_i;
  gen_data.xHodo=(G4float)xHodo_i;
  gen_data.yHodo=(G4float)yHodo_i;
  gen_data.xDet=(G4float)fDetector->PosX;
  gen_data.yDet=(G4float)fDetector->PosY;
  if(fPrintOn>0)printf("Save MC data : %f %f %f %f %f %f\n",gen_data.d_eta,gen_data.d_phi,gen_data.xHodo,gen_data.yHodo,gen_data.xDet,gen_data.yDet);
  fB_MC->Fill();
  fB_Xtal->Fill();
  
  if (EcalHC)
  {
    n_hit = EcalHC->entries();
    for (G4int i=0;i<n_hit;i++)
    {
      hit_data.iXtal=(*EcalHC)[i]->GetXtalId();
      hit_data.iType=(*EcalHC)[i]->GetType();
      hit_data.t=(*EcalHC)[i]->GetTime();
      hit_data.x=(G4float)((*EcalHC)[i]->GetPos()).x();
      hit_data.y=(G4float)((*EcalHC)[i]->GetPos()).y();
      hit_data.z=(G4float)((*EcalHC)[i]->GetPos()).z();
      hit_data.dx=(G4float)((*EcalHC)[i]->GetDir()).x();
      hit_data.dy=(G4float)((*EcalHC)[i]->GetDir()).y();
      hit_data.dz=(G4float)((*EcalHC)[i]->GetDir()).z();
      hit_data.E=(*EcalHC)[i]->GetE();
      if(((*EcalHC)[i]->GetType()==0 && fdEdxFlag=="true") ||
         ((*EcalHC)[i]->GetType()==1 && fCerenkovFlag=="true"))
        fB_Hits->Fill();
    }
  }

  
//==================================================================
//-----------------------Draw trajectories--------------------
  if (G4VVisManager::GetConcreteInstance())
  {
    G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
    G4int n_trajectories = 0;
    if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();

    for (G4int i=0; i<n_trajectories; i++) 
    {
      G4Trajectory* trj = (G4Trajectory*) ((*(evt->GetTrajectoryContainer()))[i]);
      if (fdrawFlag == "all")                                      trj->DrawTrajectory();
      else if ((fdrawFlag == "charged")&&(trj->GetCharge() != 0.)) trj->DrawTrajectory();
      else if ((fdrawFlag == "neutral")&&(trj->GetCharge() == 0.)) trj->DrawTrajectory();
    }
  }
}  
