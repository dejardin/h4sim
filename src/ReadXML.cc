#include "G4SystemOfUnits.hh"
#include "ReadXML.hh"
#include "G4ios.hh"

using namespace std;
using namespace CLHEP;
//
//--------------------------------------------------------------------
//------------------------CONSTRUCTEUR/DESTRUCTEUR-------------------------
ReadXML::ReadXML(char* geometry_path)
{
  strcpy(geometrypath,geometry_path); 
}

ReadXML::~ReadXML()
{
}

//--------------------------------------------------------------------
//------------------------Xtal size-------------------------
void ReadXML::XtalFile(FILE *Geometry)
{
  char str[132];
  sprintf(str,"Trapezoid name=");
  search(str,Geometry, 1,&Xtal_name[0]);
  
  sprintf(str,"dz=");
  parameter[0] = search(str,Geometry, 1);
  parameter[0] = units(parameter[0],Geometry);

  sprintf(str,"alp1=");
  parameter[6] = search(str,Geometry, 1);
  parameter[6] = units(parameter[6],Geometry);
  
  sprintf(str,"bl1=");
  parameter[4] = search(str,Geometry, 1); 
  parameter[4] = units(parameter[4],Geometry);
  
  sprintf(str,"tl1=");
  parameter[5] = search(str,Geometry, 1);
  parameter[5] = units(parameter[5],Geometry);
  
  sprintf(str,"h1=");
  parameter[3] = search(str,Geometry, 1);
  parameter[3] = units(parameter[3],Geometry);
  
  sprintf(str,"alp2=");
  parameter[10] = search(str,Geometry, 1);
  parameter[10] = units(parameter[10],Geometry);
  
  sprintf(str,"bl2=");
  parameter[8] = search(str,Geometry, 1);
  parameter[8] = units(parameter[8],Geometry);
  
  sprintf(str,"tl2=");
  parameter[9] = search(str,Geometry, 1);
  parameter[9] = units(parameter[9],Geometry);
  
  sprintf(str,"h2=");
  parameter[7] = search(str,Geometry, 1);
  parameter[7] = units(parameter[7],Geometry);
  
  sprintf(str,"phi=");
  parameter[2] = search(str,Geometry, 1);
  parameter[2] = units(parameter[2],Geometry);
  
  sprintf(str,"theta=");
  parameter[1] = search(str,Geometry, 1);
  parameter[1] = units(parameter[1],Geometry);
}

//--------------------------------------------------------------------
//----------------------TRANSLATIONS/ROTATIONS------------------------
void ReadXML::DirectionFile(FILE *fd)
{
  double test = -1;
  char buff[1024];
  char find2[64] = "ecal:";
  char str[132], *ret;
  
  strcpy(find,find2);
  
  strcat(find,Xtal_name);

  while (test == -1)
  {
    test = search(find,fd, 0);
    ret=fgets(buff, 1024, fd);
  }
 
  sprintf(str,"rotations:");
  search(str, fd , 0 , &rotate[0] );
  sprintf(str,"/");
  searchRotation(str,fd,&rotate[0]);
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"x=");
  translate[0] = search(str,fd, 1);
  translate[0] = units(translate[0],fd);
  
  sprintf(str,"y=");
  translate[1] = search(str,fd, 1);
  translate[1] = units(translate[1],fd);
  
  sprintf(str,"z=");
  translate[2] = search(str,fd, 1);
  translate[2] = units(translate[2],fd);
  
  RotationFile(rotate);
}
 
void ReadXML::AlveolaFile(FILE *fd)
{
  double test = -1;
  char buff[1024];
  char str[132], *ret;
  
  char find2[64] = "ecal:";
  strcpy(find,find2);
  
  strcat(find,Xtal_name);
  printf("Search for alveola : %s\n",find);
  
  while (test == -1)
  {
    test = search(find,fd, 0);
  }
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"rotations:");
  search(str,fd, 0 , &rotateAl[0] );
  ret=fgets(rotateAl,5,fd);
  
  ret=fgets(buff, 1024, fd);
  
  sprintf(str,"x=");
  translateAl[0] = search(str,fd, 1);
  translateAl[0] = units(translateAl[0],fd);
  
  sprintf(str,"y=");
  translateAl[1] = search(str,fd, 1);
  translateAl[1] = units(translateAl[1],fd);
  
  sprintf(str,"z=");
  translateAl[2] = search(str,fd, 1);
  translateAl[2] = units(translateAl[2],fd);

  RotationFile(rotateAl);
}

//--------------------------------------------------------------------
//------------------------LECTURE DES units--------------------------
double ReadXML::units(double value,FILE *tempo)
{ 
  char *ret;
  fseek(tempo, -2 , 1); 
  counter = 0;
  ret=fgets(loc_char,2,tempo);
  
  while(strcmp(loc_char,"*") != 0)
  {
   ret=fgets(loc_char,2,tempo);
  }
  
  while(strcmp(loc_char," ") != 0)
  {
   ret=fgets(loc_char,2,tempo);
   counter = counter + 1;
  }
  
  fseek(tempo, -counter , 1);
  ret=fgets(loc_char,counter-1,tempo);
  
  if (strcmp(loc_char,"mm") == 0)  value = value *mm ;
  if (strcmp(loc_char,"cm") == 0)  value = value *cm ;
  if (strcmp(loc_char,"m") == 0)  value = value *m  ;
  if (strcmp(loc_char,"mum") == 0) value = value *0.001;
  if (strcmp(loc_char,"deg") == 0) value = value *deg;
  
  return value;
}

//--------------------------------------------------------------------
//------------------------search one word--------------------------
double ReadXML::search(char* word,FILE *tempo, int sortie)
{ 
  char *ret;
  int test = 0;
  counter = 0;
  nchar = strlen(word);
  int counter_loc_char_ligne;
  double renvoi=0;
  
  ret=fgets(loc_char,nchar + 1,tempo); 
  counter_loc_char_ligne = 0;
  
  while(strcmp(loc_char,word) != 0)  
  {
    fseek(tempo, -(nchar-1) , 1);
    ret=fgets(loc_char,nchar + 1,tempo);
    counter_loc_char_ligne++;
    if (sortie == 3)  cout<<loc_char<<endl;
    if (counter_loc_char_ligne == 30) 
    {
      test = -1;
      break;
    }
  }
  
  if (sortie == 1)
  {
    fseek(tempo, 1, 1);
    ret=fgets(loc_char,2,tempo);
    counter= 1;
    while(strcmp(loc_char,"*") != 0)
    {
      ret=fgets(loc_char,2,tempo);
      counter = counter + 1;
    }
  
    fseek(tempo, -counter , 1);
    ret=fgets(loc_char,counter,tempo);
  }
  
  if (test == -1) renvoi = -1;
  if (test != -1) renvoi = atof(loc_char);
  if (sortie == 3) cout <<renvoi<<endl;
  return renvoi;
}

void ReadXML::search(char *word,FILE *tempo, int sortie,char* text)
{ 
  char *ret;
 
  int test = 0;
  int counter_loc_char_ligne;
  counter = 0;
  nchar = strlen(word);
  
  ret=fgets(loc_char,nchar + 1,tempo);
  counter_loc_char_ligne = 0;
  
  while(strcmp(loc_char,word) != 0)
  {
    //cout<<loc_char<<endl;
    fseek(tempo, -(nchar-1) , 1);
    ret=fgets(loc_char,nchar + 1,tempo);
    counter_loc_char_ligne++;
    if (counter_loc_char_ligne == 40 ) 
    {
      test = -1;
      break;
    }
  }

  if (sortie == 1)
  {
    fseek(tempo, 1, 1);
    ret=fgets(loc_char,2,tempo);
    counter= 1;
    while(strcmp(loc_char," ") != 0)
    {
      ret=fgets(loc_char,2,tempo);
      counter = counter + 1;
    }
  
    fseek(tempo, -counter , 1);
    ret=fgets(loc_char,counter-1,tempo);
  
    nchar = strlen(loc_char);
    for (int i=0;i<=nchar+1;i++)
    text[i] = loc_char[i];
  }
}

void ReadXML::searchRotation(char *word,FILE *tempo,char* text)
{
  char *ret=NULL;
  counter= 1;
  ret=fgets(loc_char,2,tempo);
  
  while(strcmp(loc_char,word) != 0)
  {
    ret=fgets(loc_char,2,tempo);
    counter = counter + 1;
  }
  
  fseek(tempo, -counter , 1);
  ret=fgets(loc_char,counter-1,tempo);
  
  nchar = strlen(loc_char);
  for (int i=0;i<=nchar+1;i++)
  text[i] = loc_char[i];
}

void ReadXML::RotationFile(char *rot)
{
  char str[132], *ret;
  char xml_file[200];
  sprintf(xml_file,"%s/rotations2.xml",geometrypath);  
  Rotation = fopen(xml_file,"rt");
  fseek(Rotation, 0, 0);
  double test = -1;
  char buff[1024];

  test = search(rot,Rotation, 0);
  while (test == -1)
  {
    ret=fgets(buff, 1024, Rotation);
    test = search(rot,Rotation, 0);
  }
  
  sprintf(str,"thetaX=");
  angles[3] = search(str,Rotation, 1);
  angles[3] = units(angles[3],Rotation);

  sprintf(str,"phiX=");
  angles[0] = search(str,Rotation, 1);
  angles[0] = units(angles[0],Rotation);
  
  sprintf(str,"thetaY=");
  angles[4] = search(str,Rotation, 1);
  angles[4] = units(angles[4],Rotation);
  
  sprintf(str,"phiY=");
  angles[1] = search(str,Rotation, 1);
  angles[1] = units(angles[1],Rotation);
  
  sprintf(str,"thetaZ=");
  angles[5] = search(str,Rotation, 1);
  angles[5] = units(angles[5],Rotation);
  
  sprintf(str,"phiZ=");
  angles[2] = search(str,Rotation, 1);
  angles[2] = units(angles[2],Rotation);
    
  fclose(Rotation);
}

//--------------------------------------------------------------------
//----------------------TRANSFERT DES valueS-------------------------
string ReadXML::XtalName()
{
  return Xtal_name;
}

double ReadXML::translate0()
{
  return translate[0];
}

double ReadXML::translate1()
{
  return translate[1];
}

double ReadXML::translate2()
{
  return translate[2];
}

double ReadXML::translateAl0()
{
  return translateAl[0];
}

double ReadXML::translateAl1()
{
  return translateAl[1];
}

double ReadXML::translateAl2()
{
  return translateAl[2];
}

double ReadXML::parm0()
{
  return parameter[0];
}

double ReadXML::parm1()
{
  return parameter[1];
}

double ReadXML::parm2()
{
  return parameter[2];
}

double ReadXML::parm3()
{
  return parameter[3];
}

double ReadXML::parm4()
{
  return parameter[4];
}

double ReadXML::parm5()
{
  return parameter[5];
}

double ReadXML::parm6()
{
  return parameter[6];
}

double ReadXML::parm7()
{
  return parameter[7];
}

double ReadXML::parm8()
{
  return parameter[8];
}

double ReadXML::parm9()
{
  return parameter[9];
}

double ReadXML::parm10()
{
  return parameter[10];
}

double ReadXML::angles0()
{
  return angles[0];
}

double ReadXML::angles1()
{
  return angles[1];
}

double ReadXML::angles2()
{
  return angles[2];
}

double ReadXML::angles3()
{
  return angles[3];
}

double ReadXML::angles4()
{
  return angles[4];
}

double ReadXML::angles5()
{
  return angles[5];
}

