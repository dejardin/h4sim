#include  "PhysicsList.hh"

#include "PhysListEmStandard.hh"
#include "G4Cerenkov.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmLowEPPhysics.hh"
#include "G4EmStandardPhysicsGS.hh"
#include "G4EmStandardPhysicsSS.hh"
#include "G4EmStandardPhysicsWVI.hh"
#include "G4DecayPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4IonPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4StoppingPhysics.hh"

#include "G4RegionStore.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"

#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Proton.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4LossTableManager.hh"
#include "StepMax.hh"

using namespace CLHEP;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//#include "G4EmProcessOptions.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PhysicsList::PhysicsList(G4int SimulCerenkov) : G4VModularPhysicsList()
{
  fSimulCerenkov   = SimulCerenkov;
  fEmPhysicsList   = NULL;
  fDecayPhysicsList= NULL;
  fCerenkovProcess = NULL;
  fStepMaxProcess  = NULL;

  G4LossTableManager::Instance();
  SetDefaultCutValue(0.1*mm);

  fStepMaxProcess = new StepMax();

  // Initilise flags
  SetVerboseLevel(1);

  fHelIsRegisted  = false;
  fBicIsRegisted  = false;
  fGnucIsRegisted = false;
  fStopIsRegisted = false;


  // EM physics
  fEmName = G4String("emstandard_opt4");
  //fEmName = G4String("emlivermore");
  fEmPhysicsList = new G4EmStandardPhysics();

  // Decay Physics is always defined
  fDecayPhysicsList = new G4DecayPhysics();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PhysicsList::~PhysicsList()
{
  delete fDecayPhysicsList;
  delete fEmPhysicsList;
  if(fCerenkovProcess)delete fCerenkovProcess;
  delete fStepMaxProcess;
  for(size_t i=0; i<fHadronPhys.size(); i++) {
    delete fHadronPhys[i];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::ConstructParticle()
{
  fDecayPhysicsList->ConstructParticle();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::ConstructProcess()
{
  AddTransportation();
  fEmPhysicsList->ConstructProcess();
  fDecayPhysicsList->ConstructProcess();
  for(size_t i=0; i<fHadronPhys.size(); ++i)
  {
    fHadronPhys[i]->ConstructProcess();
  }
  if(fSimulCerenkov)
  {
    fCerenkovProcess = new G4Cerenkov("Cerenkov");
    G4int MaxNumPhotons = 300;
    fCerenkovProcess->SetTrackSecondariesFirst(true);
    fCerenkovProcess->SetMaxBetaChangePerStep(10.0);
    fCerenkovProcess->SetMaxNumPhotonsPerStep(MaxNumPhotons);
    fCerenkovProcess->SetVerboseLevel(0);
  }
  AddStepMax();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::AddPhysicsList(const G4String& name)
{
  if (verboseLevel > 1)  
    G4cout << "PhysicsList::AddPhysicsList: <" << name << ">" << G4endl;

  if (name == fEmName) return;

  if (name == "emstandard")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysics();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emstandard_opt1")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysics_option1();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emstandard_opt2")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysics_option2();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emstandard_opt3")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysics_option3();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emstandard_opt4")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysics_option4();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emstandard_local")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new PhysListEmStandard();
    if (verboseLevel > 0)  G4cout << "PhysicsList::Set " << name << " EM physics" << G4endl;
  }
  else if (name == "emlivermore")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmLivermorePhysics();
  }
  else if (name == "empenelope")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmPenelopePhysics();
  }
  else if (name == "emlowenergy")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmLowEPPhysics();
  }
  else if (name == "emstandardGS")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysicsGS();
  }
  else if (name == "emstandardSS")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysicsSS();
  }
  else if (name == "emstandardWVI")
  {
    fEmName = name;
    delete fEmPhysicsList;
    fEmPhysicsList = new G4EmStandardPhysicsWVI();
  }
  else if (name == "elastic" && !fHelIsRegisted)
  {
    fHadronPhys.push_back( new G4HadronElasticPhysics());
    fHelIsRegisted = true;
    if (verboseLevel > 0)  G4cout << "PhysicsList::Add hadron elastic physics" << G4endl;
  }
  else if (name == "binary" && !fBicIsRegisted)
  {
    fHadronPhys.push_back(new G4HadronInelasticQBBC());
    fHadronPhys.push_back(new G4IonPhysics());
    fBicIsRegisted = true;
    if (verboseLevel > 0)  G4cout << "PhysicsList::Add hadron inelastic physics from <QBBC>" << G4endl;
  }
  else if (name == "gamma_nuc" && !fGnucIsRegisted)
  {
    fHadronPhys.push_back(new G4EmExtraPhysics());
    fGnucIsRegisted = true;
    if (verboseLevel > 0)  G4cout << "PhysicsList::Add gamma- and electro-nuclear physics" << G4endl;
  }
  else if (name == "stopping" && !fStopIsRegisted)
  {
    fHadronPhys.push_back(new G4StoppingPhysics());
    fStopIsRegisted = true;
    if (verboseLevel > 0)  G4cout << "PhysicsList::Add stopping physics" << G4endl;
  }
  else
  {
    G4cout << "PhysicsList::AddPhysicsList: <" << name << ">" 
           << " is not defined"
           << G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PhysicsList::AddStepMax()
{
  // Step limitation seen as a process

  auto particleIterator=GetParticleIterator();
  particleIterator->reset();
  while ((*particleIterator)())
  {
    G4ParticleDefinition* particle = particleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();

    if (fStepMaxProcess->IsApplicable(*particle) && !particle->IsShortLived())
    {   
      pmanager ->AddDiscreteProcess(fStepMaxProcess);
    } 
    if(fCerenkovProcess)
    {
      if (fCerenkovProcess->IsApplicable(*particle))
      {   
        G4cout << "Cerenkov process applicable on particle :" << particle->GetParticleName() << G4endl;
        pmanager->AddProcess(fCerenkovProcess);
        pmanager->SetProcessOrdering(fCerenkovProcess,idxPostStep);
      }   
    }
  }
}
