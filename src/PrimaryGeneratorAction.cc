#include "PrimaryGeneratorAction.hh"

#include "DetectorConstruction.hh"
#include "PrimaryGeneratorMessenger.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"

#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include <math.h>  

using namespace std;
using namespace CLHEP;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PrimaryGeneratorAction::PrimaryGeneratorAction(DetectorConstruction *DC)
{
  Detector=DC;
  rndmFlag="off";
  strcpy(geometrypath,"xml");

  G4int n_particle = 1;
  particleGun  = new G4ParticleGun(n_particle);

  //*******************************************************
  //************* Angles values for the test beam *********
  //**************** (From Givernaud) *********************

  // *** JG : Read tab_eta & tab_phi values from file ***

  char fpath[1024] ;
  sprintf(fpath,"%s/tabEta",geometrypath) ;
  float value = 0. ;

  ifstream fin ;
  fin.open(fpath) ;
  int count = 0 ;
  char line[80] ;
  if(fin.is_open()) {
    while(!fin.getline(line,80).eof())
    {
      if (line[0] != 't') continue ; // skip white lines
      sscanf(line,"%*s %f",&value) ;
      if(count < 85 ) tab_eta[count] = value ;
      count++ ;
    }
    fin.close() ;
  }
  else
  {
    cerr << "can't open tabEta file" << endl ;
  }

  sprintf(fpath,"%s/tabPhi",geometrypath) ;
  fin.open(fpath) ;
  count = 0 ;
  if(fin.is_open())
  {
    while(!fin.getline(line,80).eof()) {
      if (line[0] != 't') continue ; // skip white lines
      sscanf(line,"%*s %f",&value) ;
      if(count < 20) tab_phi[count] = value ;
      count++ ;
    }
    fin.close() ;
  }
  else
  {
    cerr << "can't open tabPhi file" << endl ;
  }

  //create a messenger for this class
  gunMessenger = new PrimaryGeneratorMessenger(this);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle = particleTable->FindParticle(particleName="e-");
  particleGun->SetParticleDefinition(particle);

  compteur_eta = 0;
  ref_eta = 0;
  cout << "Reading centers file" << endl;

  //*** READING GEOMETRY CONFIGURATION FROM GEOMETRY.DUMP.CENTER FILE ***//
  // *** JG : center_file path as local member, build just once in constructor.

  ifstream geometry;
  sprintf(center_file,"%s/geometry.dump.center",geometrypath);
  geometry.open(center_file);
  if (!geometry)
  {
    cerr << "mpg : Error reading file " << center_file << endl;
    exit(-1);
  }
  char matchstring[100];

  //phi index is 360-pos_phi_init in geometry.dump.center file
  //eta index is pos_eta_init-1 in geometry.dump.center file
  // Don't use it for TB_2018 : add (-1, -1) position to get 0. 0. 152. direction.
  // We move the table instead of the beam.

#ifdef TB_2018
  sprintf(matchstring,"eta=%d phi=%d",pos_eta_init,pos_phi_init);
#else
  sprintf(matchstring,"eta=%d phi=%d",pos_eta_init,340+pos_phi_init);
#endif

  while(!geometry.eof())
  {
    geometry.getline(line,100);
    if (strcmp(line,matchstring)==0)
    {
      //      cout "Found center for crystal eta " << pos_eta_init << " phi " << pos_phi_init << endl;
      geometry >> center_position;
      cout << "Center Position is " << center_position << endl;
      break;
    }
  }
  geometry.close();
  center_position.setMag(center_position.mag()*10.*mm);
}

void PrimaryGeneratorAction::SetposXY(G4String val)
{
  G4int iret;
  G4double loc_posx=0., loc_posy=0.;

#ifndef TB_2018
  G4cout << "Set X and Y not available full SM test beam. Use SetPosEtaPhi instead to move the table." << G4endl;
#else
  char cloc_unit[80];
  iret=sscanf(val,"%lf %lf %s",&loc_posx, &loc_posy, cloc_unit);
  G4String loc_unit=cloc_unit;
  if(iret==2) loc_unit="cm";
  if     (loc_unit=="m")  {loc_posx*=m;  loc_posy*=m;}
  else if(loc_unit=="cm") {loc_posx*=cm; loc_posy*=cm;}
  else if(loc_unit=="mm") {loc_posx*=mm; loc_posy*=mm;}
#endif

  pos_X_init=loc_posx;
  pos_Y_init=-loc_posy;
}

void PrimaryGeneratorAction::SetposEtaPhi(G4String val)
{ 
  G4int iret, loc_eta=0, loc_phi=0;
  iret=sscanf(val,"%d %d",&pos_eta_init, &pos_phi_init);

#ifdef TB_2018
  G4cout << "Set Eta and phi not available for 2018 TB. Use SetPosXY instead to move the Crystal matrix." << G4endl;
#else
  ifstream geometry;
  geometry.open(center_file);

  if (!geometry)
  {
    cerr << "mpg SetposEtaPhi : Error reading file " << center_file << endl;
    exit(-1);
  }

  char line[100];
  char matchstring[100];

  sprintf(matchstring,"eta=%d phi=%d",pos_eta_init,340+pos_phi_init);
  while(!geometry.eof())
  {
    geometry.getline(line,100);
    if (strcmp(line,matchstring)==0)
    {
      G4cout << "Found center for crystal eta " << pos_eta_init << " phi " << pos_phi_init << G4endl;
      geometry >> center_position;
      G4cout << "Center Position is " << center_position << G4endl;
      break;
    }
  }
  geometry.close();
  center_position.setMag(center_position.mag()*10.*mm);
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
  delete gunMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4RotationMatrix PrimaryGeneratorAction::ConversionMatrix(G4double theta,G4double phi,G4double psi)
{
  G4double pi=2.*asin(1.0l);
  G4RotationMatrix A;
  A.rotateZ(phi);

  G4RotationMatrix B;
  B.rotateY(theta);

  G4RotationMatrix C;
  C.rotateZ(psi);

  G4RotationMatrix D;
  D.rotateZ(-pi);

  return A*B*C*D;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4String name ;

  G4ThreeVector vertex(0.,0.,0.);

  G4cout << "Beam type : " << beamType << G4endl;
  if (beamType=="center")
  {
    //************ i.e. to shoot in the center of xtal front face *********************
    //It is also possible to set dtheta and dphi
    G4ThreeVector direction=center_position;
    
    direction.setTheta(center_position.getTheta()-d_theta);
    direction.setPhi(center_position.getPhi()-d_phi);
    
    vertex=center_position-direction;
    particleGun->SetParticleMomentumDirection(direction);
  }
  else if (beamType=="testbeam")
  {
    //************ i.e. to shoot as it is done in the testbeam *********************
    // For the moment it is not possible to set dtheta and dphi
   
    G4double phi=GetTabPhi(pos_phi_init)*deg;
    G4double theta=GetTabEta(pos_eta_init)*deg;
    // G4double eta=-log(tan(theta/2));
    
    G4double x0 = sin(theta)*cos(phi); 
    G4double y0 = sin(theta)*sin(phi);
    G4double z0 = cos(theta);
    
#ifndef TB_2018
    particleGun->SetParticleMomentumDirection(G4ThreeVector(x0,y0,z0));
#endif
  }
//**************** Beam Profile Simulation ******************
  //phi direction is Y (positive with positive xtal numbering in phi)
  //eta direction is X (positive with positive xtal numbering in eta)
  if (beamProfile=="flat")
  {
    yHodo=RandFlat::shoot(beam_y_min,beam_y_max)/mm;
    xHodo=RandFlat::shoot(beam_x_min,beam_x_max)/mm;
//     cout << beam_y_min << " , " << beam_y_max << " , " << yHodo << endl;  
//     cout << beam_x_min << " , " << beam_x_max << " , " << xHodo << endl;  
  }
  else if (beamProfile=="gauss")
  {
    yHodo=(beam_y_mean+(RandGauss::shoot()*mm*beam_y_sigma))/mm;
    xHodo=(beam_x_mean+(RandGauss::shoot()*mm*beam_x_sigma))/mm;
  }
  else if (beamProfile=="point")
  {
    yHodo=0.;
    xHodo=0.;
  }
 
  yHodo = yHodo+(delta_y/mm) ;
  xHodo = xHodo+(delta_x/mm) ;

  G4ThreeVector beam_hodo(xHodo*mm,yHodo*mm,0.*mm);
  
  G4double psi=0.;
  
  G4RotationMatrix convert=ConversionMatrix(particleGun->GetParticleMomentumDirection().theta(),particleGun->GetParticleMomentumDirection().phi(),psi);

  G4ThreeVector beam_geant=convert*beam_hodo;

  G4ThreeVector pos_XY(0.,pos_Y_init, pos_X_init);  
  G4ThreeVector Offset=beam_geant+vertex+pos_XY;
  particleGun->SetParticlePosition(Offset); 
  
  G4cout << Offset << endl;

  G4ThreeVector true_dir=particleGun->GetParticleMomentumDirection()/mm;
  true_dir.setMag(center_position.mag()*mm);

  G4cout << "Direction is " << particleGun->GetParticlePosition() << "->"  << true_dir+particleGun->GetParticlePosition() <<  endl; 

  particleGun->GeneratePrimaryVertex(anEvent);
  
  double Einit =particleGun->GetParticleEnergy();
  
  name = particleGun->GetParticleDefinition()->GetParticleName() ;  
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



