#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"



#include "CalorimeterSD.hh"
#include "G4AssemblyVolume.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4Trap.hh"
#include "ReadXML.hh"
#include "Rotation.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4VSolid.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

using namespace CLHEP;
//--------------------------------------------------CONSTRUCTEUR----------------------------------

DetectorConstruction::DetectorConstruction()
{
  // default parameter values of the calorimeter
  strcpy(geometrypath,"xml");
#ifdef TB_2018
  WorldSizeYZ  = 3.  * m;
  WorldSizeX   = 3.  * m; 
#else
  WorldSizeYZ  = 6.  * m;
  WorldSizeX   = 6.  * m; 
#endif
  visu = 1111;
  VisAlu = 0;  
  PosX = PosX_ref;
  PosY = PosY_ref;
  ModTheta = ModTheta_ref;
  ModPhi = ModPhi_ref;
  detectorMessenger = new DetectorMessenger(this);
  G4GeometryManager::GetInstance()->SetWorldMaximumExtent(WorldSizeX);
}




//--------------------------------------------------DESTRUCTEUR----------------------------------

DetectorConstruction::~DetectorConstruction()
{ 
  delete detectorMessenger;
}

//--------------------------------------------------CONSTRUCTION CALORIMETRE----------------------------------
G4VPhysicalVolume* DetectorConstruction::Construct()
{
  DefineMaterials();
  return ConstructCalorimeter();
}

//--------------------------------------------------MATERIAUX----------------------------------
void DetectorConstruction::DefineMaterials()
{ 
  G4double a,z,density;

  G4NistManager* man = G4NistManager::Instance();
  //  man->SetVerbose(1);
  WorldMaterial   = man->FindOrBuildMaterial("G4_AIR");
  FrameMaterial   = man->FindOrBuildMaterial("G4_Al");
  CrystalMaterial = man->FindOrBuildMaterial("G4_PbWO4");
  defaultMaterial = WorldMaterial;

  const G4int NENTRIES = 2;  
  G4double ppckov[NENTRIES] =     {1.5*eV, 3.5*eV};
  G4double rindex[NENTRIES] =     {2.24,   2.24};
  G4double absorption[NENTRIES] = {300*cm, 300*cm};
  G4MaterialPropertiesTable *MPT = new G4MaterialPropertiesTable();
  MPT->AddProperty("RINDEX",ppckov,rindex,NENTRIES);
  //MPT->AddProperty("ABSLENGTH",ppckov,absorption,NENTRIES);
  CrystalMaterial->SetMaterialPropertiesTable(MPT);

  a = 1.01*g/mole;
  G4Element* H  = new G4Element("Hydrogen","H" , 1., a);
  a = 12.01*g/mole;
  G4Element* C  = new G4Element("Carbon"  ,"C" , 6., a);
  a = 14.01*g/mole;
  G4Element* N  = new G4Element("Nitrogen","N" , 7., a);
  density = 1.43*mg/cm3;
  a = 15.999*g/mole;
  G4Element* O  = new G4Element("Oxygen"  ,"O" , 8., a);
  density = 2.34*g/cm3;
  a = 10*g/mole;
  G4Element* B10  = new G4Element("Bore 10"  ,"B10" , 5., a);
  density = 2.34*g/cm3;
  a = 11*g/mole;
  G4Element* B11  = new G4Element("Bore 11"  ,"B11" , 5., a);
  density = 969*mg/cm3;
  a = 22.99*g/mole;
  G4Element* Na  = new G4Element("Sodium"  ,"Na" , 11., a);
  density = 2.33*g/cm3;
  a = 28.09*g/mole;
  G4Element* Si = new G4Element("Silicon","Si" , 14., a );
  density = 2.7*g/cm3;
  a = 26.98*g/mole;
  G4Element* Al = new G4Element("Aluminium","Al" , 13., a );
  density = 2.07*g/cm3;
  G4Material* AlveolaGlassFiber = new G4Material("Alveola Mix"  , density, 8);
  AlveolaGlassFiber->AddElement(Si, 0.36611059*0.48);
  AlveolaGlassFiber->AddElement(O, 0.53173295*0.48+0.33280996*0.29);
  AlveolaGlassFiber->AddElement(B10, 0.007820091*0.48);
  AlveolaGlassFiber->AddElement(B11, 0.0344084*0.48);
  AlveolaGlassFiber->AddElement(Na, 0.059927964*0.48);
  AlveolaGlassFiber->AddElement(Al, 0.23);
  AlveolaGlassFiber->AddElement(H,  0.13179314*0.29);
  AlveolaGlassFiber->AddElement(C,  0.53539691*0.29);
  AlveolaMaterial = AlveolaGlassFiber;

}

//-------------------------------------------DEFINITION DES VOLUMES-------------------------------
G4VPhysicalVolume* DetectorConstruction::ConstructCalorimeter()
{
//======================================================================     
//------------------------------------World-----------------------------
//======================================================================
  solidWorld = new G4Box("World", //its name
                         WorldSizeX/2,WorldSizeYZ/2,WorldSizeYZ/2);          //its size
                   
  logicalWorld = new G4LogicalVolume(solidWorld, //its solid
                             defaultMaterial,//its material
                             "World");       //its name
                             
  physicalWorld = new G4PVPlacement(0, //no rotation
                           G4ThreeVector(), //at (0,0,0)
                           "World", //its name
                           logicalWorld, //its logical volume
                           0, //its mother  volume
                           false, //no boolean operation
                           0);              //copy number
  
  
//======================================================================
//--------------------Plates d'aluminium
//======================================================================
#ifndef TB_2018
  G4ThreeVector   T_pl;
  Rotation     transf_pl;
  G4ThreeVector    colX_pl;
  G4ThreeVector    colY_pl;
  G4ThreeVector    colZ_pl;
  G4RotationMatrix   R_pl;
  G4Transform3D      direction_pl;
  
  solidPlateInfCD = new G4Box("PlateInfCD",15.189*cm,1*mm,2*cm);      
  
  logicalPlateInfCD = new G4LogicalVolume(solidPlateInfCD, FrameMaterial, "PlateInfCD");      
       
  T_pl.setX(1.3825*m);
  T_pl.setY(22.7727*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m );
   
  transf_pl.position_rot(-167*deg,0*deg,103*deg, 90*deg,0*deg,90*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalPlateInfCD = new G4PVPlacement( direction_pl, "PlateInfCD", logicalPlateInfCD, physicalWorld, false, 0);      

//---------------------------------------------------------------------
  solidPlateInfCG = new G4Box("PlateInfCG",15.169*cm,1*mm,1.5*cm);      
  logicalPlateInfCG = new G4LogicalVolume(solidPlateInfCG, FrameMaterial, "PlateInfCG");      
     
  T_pl.setX(1.38414*m);
  T_pl.setY(-21.8035*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,0*deg,83*deg, 90*deg,0*deg,90*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalPlateInfCG = new G4PVPlacement(  direction_pl, "PlateInfCG", logicalPlateInfCG, physicalWorld, false, 0);

//--------------------------------------------------------------------------
  solidPlateInfP = new G4Tubs("PlateInfP",1.25*m,1.273*m,1.342*m,-9*deg,18*deg);      
  logicalPlateInfP = new G4LogicalVolume(solidPlateInfP, FrameMaterial, "PlateInfP");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -1.5185*m + 1.52 * m );
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
#ifndef TB_2018
  physicalPlateInfP = new G4PVPlacement(  direction_pl, "PlateInfP", logicalPlateInfP, physicalWorld, false, 0); 
#endif

//--------------------------------------------------------------------------
  solidPlateInfP2 = new G4Tubs("PlateInfP2",1.273*m,1.277*m,1.342*m,-9.8999996*deg,19.6499996*deg);      
  logicalPlateInfP2 = new G4LogicalVolume(solidPlateInfP2, FrameMaterial, "PlateInfP2");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -17.53 * cm + 1.52 * m );
 
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
   R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
   direction_pl = G4Transform3D(R_pl,T_pl);
   physicalPlateInfP2 = new G4PVPlacement(  direction_pl, "PlateInfP2", logicalPlateInfP2, physicalWorld, false, 0); 

//--------------------------------------------------------------------------
  solidPlateInfP3 = new G4Tubs("PlateInfP3",1.248*m,1.273*m,1.342*m,-9.8999996*deg,19.6499996*deg);      
  logicalPlateInfP3 = new G4LogicalVolume(solidPlateInfP3, FrameMaterial, "PlateInfP3");      
     
  T_pl.setX(0*m);
  T_pl.setY(0*cm);
  T_pl.setZ( -17.53*cm + 1.52 * m  );
 
  transf_pl.position_rot(0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalPlateInfP3 = new G4PVPlacement(  direction_pl, "PlateInfP3", logicalPlateInfP3, physicalWorld, false, 0);         

//--------------------------------------------------------------------------
  solidE1CD = new G4Box("E1CD",15.5395*cm,0.5*mm,35.675*cm);      
  logicalE1CD = new G4LogicalVolume(solidE1CD, FrameMaterial, "E1CD");      
     
  T_pl.setX(1.37769*m);
  T_pl.setY(24.6607*cm);
  T_pl.setZ( -1.16075*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE1CD = new G4PVPlacement(direction_pl, "E1CD", logicalE1CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE1CG = new G4Box("E1CG",15.5373*cm,0.5*mm,35.675*cm);      
  logicalE1CG = new G4LogicalVolume(solidE1CG, FrameMaterial, "E1CG");      
     
  T_pl.setX(1.37965*m);
  T_pl.setY(-23.5444*cm);
  T_pl.setZ( -1.16075*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE1CG = new G4PVPlacement(direction_pl, "E1CG", logicalE1CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE1CN = new G4Cons("E1CN",1.27511*m,1.28591*m, //rmin1,rmax1
                                1.54319*m,1.554*m, //rmin1,rmax1
                                5.3405*cm, //dz
                               -9.3999996*deg,19.25*deg); //startphi,deltaphi     

  logicalE1CN = new G4LogicalVolume( solidE1CN, FrameMaterial, "E1CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -87.7717*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE1CN = new G4PVPlacement(direction_pl, "E1CN", logicalE1CN, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE1EC = new G4Tubs("E1EC", 1.2444*m, 1.2446*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicalE1EC = new G4LogicalVolume(solidE1EC, FrameMaterial, "E1EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE1EC = new G4PVPlacement(direction_pl, "E1EC", logicalE1EC, physicalWorld, false, 0 );        
 
//--------------------------------------------------------------------------
  solidE2CD = new G4Box("E2CD",14.6906*cm,0.5*mm,14.4327*cm);      
  logicalE2CD = new G4LogicalVolume(solidE2CD, FrameMaterial, "E2CD");      
     
  T_pl.setX(1.36942*m);
  T_pl.setY(24.4698*cm);
  T_pl.setZ( -65.9673*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE2CD = new G4PVPlacement(direction_pl, "E2CD", logicalE2CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE2CG = new G4Box("E2CG",14.6885*cm,0.5*mm,14.4327*cm);      
  logicalE2CG = new G4LogicalVolume(solidE2CG, FrameMaterial, "E2CG");      
     
  T_pl.setX(1.37122*m);
  T_pl.setY(-23.441*cm);
  T_pl.setZ( -65.9673*cm + 1.52 * m);
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE2CG = new G4PVPlacement(direction_pl, "E2CG", logicalE2CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE2CN = new G4Cons("E2CN",1.27511*m,1.28155*m, //rmin1,rmax1
                        1.51585*m,1.5223*m, //rmin1,rmax1
                        9.5142*cm, //dz
                       -9.3999996*deg,19.25*deg); //startphi,deltaphi     
  logicalE2CN = new G4LogicalVolume( solidE2CN, FrameMaterial, "E2CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -30.0812*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE2CN = new G4PVPlacement(direction_pl, "E2CN", logicalE2CN, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE2EC = new G4Tubs("E2EC", 1.2446*m, 1.2476*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicalE2EC = new G4LogicalVolume(solidE2EC, FrameMaterial, "E2EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE2EC = new G4PVPlacement(direction_pl, "E2EC", logicalE2EC, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CD = new G4Box("E3CD",13.9504*cm,0.5*mm,16.9173*cm);      
  logicalE3CD = new G4LogicalVolume(solidE3CD, FrameMaterial, "E3CD");      
     
  T_pl.setX(1.36221*m);
  T_pl.setY(24.3032*cm);
  T_pl.setZ( -34.6173*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE3CD = new G4PVPlacement(direction_pl, "E3CD", logicalE3CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CG = new G4Box("E3CG",13.9483*cm,0.5*mm,16.9173*cm);      
  logicalE3CG = new G4LogicalVolume(solidE3CG, FrameMaterial, "E3CG");      
     
  T_pl.setX(1.36387*m);
  T_pl.setY(-23.3508*cm);
  T_pl.setZ( -34.6173*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());

  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE3CG = new G4PVPlacement(direction_pl, "E3CG", logicalE3CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE3CN = new G4Cons("E3CN",1.27511*m,1.28022*m, //rmin1,rmax1
                          1.48239*m,1.4875*m, //rmin1,rmax1
                          13.059*cm, //dz
                          -9.3999996*deg,19.25*deg); //startphi,deltaphi     
  logicalE3CN = new G4LogicalVolume( solidE3CN, FrameMaterial, "E3CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( 40.7902*cm + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE3CN = new G4PVPlacement(direction_pl, "E3CN", logicalE3CN, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE3EC = new G4Tubs("E3EC", 1.2476*m, 1.2478*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicalE3EC = new G4LogicalVolume(solidE3EC, FrameMaterial, "E3EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE3EC = new G4PVPlacement(direction_pl, "E3EC", logicalE3EC, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE4CD = new G4Box("E4CD",13.0985*cm,0.5*mm,16.3281*cm);      
  logicalE4CD = new G4LogicalVolume(solidE4CD, FrameMaterial, "E4CD");      
     
  T_pl.setX(1.35391*m);
  T_pl.setY(24.1116*cm);
  T_pl.setZ( -1.37194*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
      
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE4CD = new G4PVPlacement(direction_pl, "E4CD", logicalE4CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE4CG = new G4Box("E4CG",13.0965*cm,0.5*mm,16.3281*cm);      
  logicalE4CG = new G4LogicalVolume(solidE4CG, FrameMaterial, "E4CG");      
     
  T_pl.setX(1.35542*m);
  T_pl.setY(-23.2469*cm);
  T_pl.setZ( -1.37194*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE4CG = new G4PVPlacement(direction_pl, "E4CG", logicalE4CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE4CN = new G4Cons("E4CN",1.27511*m,1.27738*m, //rmin1,rmax1
                                1.45193*m,1.4542*m, //rmin1,rmax1
                                16.3281*cm, //dz
                                -9.3999996*deg,19.25*deg);  //startphi,deltaphi     
  logicalE4CN = new G4LogicalVolume( solidE4CN, FrameMaterial, "E4CN" );      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( 1.32974*m + 1.52 * m );
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE4CN = new G4PVPlacement(direction_pl, "E4CN", logicalE4CN, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE4EC = new G4Tubs("E4EC", 1.2478*m, 1.248*m, 1.342*m, -9.8999996*deg, 19.6499996*deg );      
  logicalE4EC = new G4LogicalVolume(solidE4EC, FrameMaterial, "E4EC");      
     
  T_pl.setX( 0.0 * m );
  T_pl.setY( 0.0 * cm );
  T_pl.setZ( -17.53 * cm + 1.52 * m);
 
  transf_pl.position_rot( 0*deg,90*deg,0*deg, 90*deg,90*deg,0*deg );
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE4EC = new G4PVPlacement(direction_pl, "E4EC", logicalE4EC, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE5CD = new G4Box("E5CD",12.2115*cm,0.5*mm,21.2719*cm);      
  logicalE5CD = new G4LogicalVolume(solidE5CD, FrameMaterial, "E5CD");      
     
  T_pl.setX(1.34526*m);
  T_pl.setY(23.9121*cm);
  T_pl.setZ( 36.2281*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE5CD = new G4PVPlacement(direction_pl, "E5CD", logicalE5CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE5CG = new G4Box("E5CG",12.2097*cm,0.5*mm,21.2719*cm);      
  logicalE5CG = new G4LogicalVolume(solidE5CG, FrameMaterial, "E5CG");      
     
  T_pl.setX(1.34662*m);
  T_pl.setY(-23.1389*cm);
  T_pl.setZ( 36.2281*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE5CG = new G4PVPlacement(direction_pl, "E5CG", logicalE5CG, physicalWorld, false, 0 );
      
//--------------------------------------------------------------------------
  solidE6CD = new G4Box("E6CD",11.3485*cm,0.5*mm,20.3906*cm);      
  logicalE6CD = new G4LogicalVolume(solidE6CD, FrameMaterial, "E6CD");      
     
  T_pl.setX(1.33686*m);
  T_pl.setY(23.7179*cm);
  T_pl.setZ( 77.8906*cm + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE6CD = new G4PVPlacement(direction_pl, "E6CD", logicalE6CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE6CG = new G4Box("E6CG",11.3468*cm,0.5*mm,20.3906*cm);      
  logicalE6CG = new G4LogicalVolume(solidE6CG, FrameMaterial, "E6CG");      

  T_pl.setX(1.33805*m);
  T_pl.setY(-23.0337*cm);
  T_pl.setZ( 77.8906*cm + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE6CG = new G4PVPlacement(direction_pl, "E6CG", logicalE6CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE7CD = new G4Box("E7CD",10.5446*cm,0.5*mm,9.68438*cm);      
  logicalE7CD = new G4LogicalVolume(solidE7CD, FrameMaterial, "E7CD");      
     
  T_pl.setX(1.32902*m);
  T_pl.setY(23.5371*cm);
  T_pl.setZ( 1.07966*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE7CD = new G4PVPlacement(direction_pl, "E7CD", logicalE7CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE7CG = new G4Box("E7CG",10.543*cm,0.5*mm,9.68438*cm);      
  logicalE7CG = new G4LogicalVolume(solidE7CG, FrameMaterial, "E7CG");      
     
  T_pl.setX(1.33007*m);
  T_pl.setY(-22.9357*cm);
  T_pl.setZ( 1.07966*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE7CG = new G4PVPlacement(direction_pl, "E7CG", logicalE7CG, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE8CD = new G4Trap("E8CD", 17.35*cm,
                                 16.90296*deg, 180*deg,
                                 0.5*mm, 10.5446*cm, 10.5446*cm,
                                 0*deg,
                                 0.5*mm, 0.000001*mm, 0.000001*mm,
                                 0*deg );      

  logicalE8CD = new G4LogicalVolume(solidE8CD, FrameMaterial, "E8CD");      
     
  T_pl.setX(1.38039*m);
  T_pl.setY(24.7231*cm);
  T_pl.setZ( 1.35*m + 1.52 * m);
 
  transf_pl.position_rot(-167*deg,-77*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE8CD = new G4PVPlacement(direction_pl, "E8CD", logicalE8CD, physicalWorld, false, 0 );

//--------------------------------------------------------------------------
  solidE8CG = new G4Trap("E8CG", 17.35*cm,
                                 16.90053*deg, 180*deg,
                                 0.5*mm, 10.543*cm, 10.543*cm,
                                 0*deg,
                                 0.5*mm, 0.000001*mm, 0.0000001*mm,
                                 0*deg );      
  logicalE8CG = new G4LogicalVolume(solidE8CG, FrameMaterial, "E8CG");      
     
  T_pl.setX(1.3824*m);
  T_pl.setY(-23.5782*cm);
  T_pl.setZ( 1.35*m + 1.52 * m);
 
  transf_pl.position_rot(173*deg,-97*deg,0*deg, 90*deg,90*deg,0*deg);
  colX_pl.setX(transf_pl.XXr());
  colX_pl.setY(transf_pl.YXr());
  colX_pl.setZ(transf_pl.ZXr());
  colY_pl.setX(transf_pl.XYr());
  colY_pl.setY(transf_pl.YYr());
  colY_pl.setZ(transf_pl.ZYr());
  colZ_pl.setX(transf_pl.XZr());
  colZ_pl.setY(transf_pl.YZr());
  colZ_pl.setZ(transf_pl.ZZr());
  
  R_pl = HepRotation ( colX_pl , colY_pl , colZ_pl);
  direction_pl = G4Transform3D(R_pl,T_pl);
  physicalE8CG = new G4PVPlacement(direction_pl, "E8CG", logicalE8CG, physicalWorld, false, 0 );
#endif

//======================================================================                           
// --------------------Cristaux et alv�oles
//======================================================================
  ReadXML geom(geometrypath);
  char ligne[1024];
  G4RotationMatrix Ra;
  G4RotationMatrix RAl;
  G4RotationMatrix Rc;
  G4RotationMatrix Rtot;
  G4ThreeVector Ta;
  G4ThreeVector TAl;
  G4ThreeVector Ttot;
  G4Transform3D    transf;
  Rotation   transf2;
  G4Transform3D    transfAl;
  G4ThreeVector  colX;
  G4ThreeVector  colY;
  G4ThreeVector  colZ;
  G4ThreeVector  colXAl;
  G4ThreeVector  colYAl;
  G4ThreeVector  colZAl;
  int num_al;

  char xml_file[200];
#ifdef TB_2018
  sprintf(xml_file,"%s/alveoles_2018.xml",geometrypath);
#else
  sprintf(xml_file,"%s/alveoles.xml",geometrypath);
#endif

  Geometry = fopen(xml_file,"rt");
  int j=0;

  for(int n = 0; n<nb_alveola ; n++)
  {
// Read next alveola name and geometry (G4Trap parameters)
    geom.XtalFile(Geometry);
  
#ifdef TB_2018
    sprintf(xml_file,"%s/ecal_2018.xml",geometrypath);
#else
    sprintf(xml_file,"%s/ecal2.xml",geometrypath);
#endif
    Alveola = fopen(xml_file,"rt");

    for(int i = 0; i<alveola_stack ; i++)
    {
// Read associated alveola positions (Stack of 10 alveola in a SM)
      geom.AlveolaFile(Alveola);
     
      TAl.setX(geom.translateAl0());
      TAl.setY(geom.translateAl1() + PosY);
      TAl.setZ(geom.translateAl2() + 1.52 * m - PosX);
      printf("Alveola %d, stack %d : %d, translation %+5.7e %+5.7e %+5.7e\n",n,i,j,geom.translateAl0(),geom.translateAl1(),geom.translateAl2() + 1.52 * m);

      transf2.position_rot(geom.angles0(),geom.angles1(),geom.angles2(),
                           geom.angles3(),geom.angles4(),geom.angles5());
      colXAl.setX(transf2.XXr());
      colXAl.setY(transf2.YXr());
      colXAl.setZ(transf2.ZXr());
      colYAl.setX(transf2.XYr());
      colYAl.setY(transf2.YYr());
      colYAl.setZ(transf2.ZYr());
      colZAl.setX(transf2.XZr());
      colZAl.setY(transf2.YZr());
      colZAl.setZ(transf2.ZZr());
      RAl = HepRotation ( colXAl , colYAl , colZAl);

#ifdef TB_2018
      //printf("Rotation : tx %.4f, px %.4f, ty %.4f, py %.4f, tz %.4f, pz %.4f\n",
      //        RAl.thetaX()/deg, RAl.phiX()/deg,RAl.thetaY()/deg, RAl.phiY()/deg,RAl.thetaZ()/deg, RAl.phiZ()/deg);
// For TB 2018, the bottom of the second alveola is parallel to horizontal
      RAl.rotateZ(ModPhi);
// And, the first column of crystals was parallel to beam line
      RAl.rotateY(ModTheta);
      //printf("Rotation : tx %.4f, px %.4f, ty %.4f, py %.4f, tz %.4f, pz %.4f\n",
      //        RAl.thetaX()/deg, RAl.phiX()/deg,RAl.thetaY()/deg, RAl.phiY()/deg,RAl.thetaZ()/deg, RAl.phiZ()/deg);
#endif
      transfAl = G4Transform3D(RAl,TAl);
      solidAlveola[j]    = NULL;
      logicalAlveola[j]  = NULL;
      physicalAlveola[j] = NULL;
      solidAlveola[j]    = new G4Trap( geom.XtalName(), geom.parm0(), geom.parm1(), geom.parm2(), geom.parm3(), geom.parm4(),
                                       geom.parm5(), geom.parm6(), geom.parm7(), geom.parm8(), geom.parm9(), geom.parm10());

      logicalAlveola[j]  = new G4LogicalVolume( solidAlveola[j], AlveolaMaterial, geom.XtalName(), 0,0,0);
      physicalAlveola[j] = new G4PVPlacement( transfAl,          // its position
                                              geom.XtalName(),   // its name
                                              logicalAlveola[j], // its logical volume
                                              physicalWorld,     // its mother
                                              false,             // no boolean operat
                                              0                  // copy number
                                            );                     

      // vect.push_back(physicalAlveola[j]);
      j++;
    }
    fclose(Alveola);
    fgets(ligne, 1024, Geometry);
  }
  fclose(Geometry);
  printf("Alveola definition done !\n");
  
#ifdef TB_2018
  sprintf(xml_file,"%s/cristal_2018.xml",geometrypath);
#else
  sprintf(xml_file,"%s/cristal.xml",geometrypath);
#endif
  Geometry = fopen(xml_file,"rt");
  
  j=0;
  for(int n = 0; n<nb_alveola*5*2 ; n++)
  {
// 170 =17*5*2 = 17 alveola types * 5 crystals per alveola * 2 R/L cystals
    int loc_alveola_type=n/10;

// Read crystal name and geometry (G4Trap parameters)
    geom.XtalFile(Geometry);

#ifdef TB_2018
    sprintf(xml_file,"%s/ecal_2018.xml",geometrypath);
#else
    sprintf(xml_file,"%s/ecal2.xml",geometrypath);
#endif
    Direction = fopen(xml_file,"rt");  
// And associated position (search for crystal name in file, read translation and rotation)
    geom.DirectionFile(Direction);
    fclose(Direction);
     
    Ta.setX(geom.translate0());
    Ta.setY(geom.translate1());
    Ta.setZ(geom.translate2());
    transf2.position_rot(geom.angles0(),geom.angles1(),geom.angles2(),
                         geom.angles3(),geom.angles4(),geom.angles5());
  
    colX.setX(transf2.XXr());
    colX.setY(transf2.YXr());
    colX.setZ(transf2.ZXr());
    colY.setX(transf2.XYr());
    colY.setY(transf2.YYr());
    colY.setZ(transf2.ZYr());
    colZ.setX(transf2.XZr());
    colZ.setY(transf2.YZr());
    colZ.setZ(transf2.ZZr());
     
    Ra = HepRotation ( colX , colY , colZ);
    transf = G4Transform3D(Ra,Ta); 
       
    G4cout << "Crystal Rotation:    X=" << colX << "; Y= " << colY << "; Z= " << colZ << G4endl;
    G4cout << "Crystal Translation: " << Ta << G4endl;

    for(int i = 0; i<alveola_stack ; i++)
    {
      num_al = i+loc_alveola_type*alveola_stack;
     
  
//Crystal Rotation:    X=(1,0,6.12323e-17); Y= (5.61614e-17,0.917185,0.398461); Z= (2.43987e-17,-0.398461,0.917185)
//Crystal Translation: (12.012,2.393,-3.97)
//Creating solid crystal 12 : deg = 1.745329e-02, tolerance : 3.000000e-08
//dz=+1.15000e+02, theta=+1.10655e-02, phi=-7.80368e-01,
// dy_zm=+1.13005e+01, dx_ym_zm=+1.09154e+01, dx_yp_zm=+1.09996e+01, alpha_zm=+3.72507e-03,
// dy_zp=+1.30910e+01, dx_ym_zp=+1.27173e+01, dx_yp_zp=+1.28149e+01, alpha_zp=+3.72507e-03
//Defining crystal j=12, n=12, i=0, alveola=1

      printf("Creating solid crystal %d : deg = %e, tolerance : %e\n",j,deg,G4GeometryTolerance::GetInstance()->GetSurfaceTolerance());
      printf("dz=%+7.5e, theta=%+7.5e, phi=%+7.5e,\n"
             " dy_zm=%+7.5e, dx_ym_zm=%+7.5e, dx_yp_zm=%+7.5e, alpha_zm=%+7.5e,\n dy_zp=%+7.5e, dx_ym_zp=%+7.5e, dx_yp_zp=%+7.5e, alpha_zp=%+7.5e\n",
              geom.parm0(), geom.parm1(), geom.parm2(),
              geom.parm3(), geom.parm4(), geom.parm5(), geom.parm6(),
              geom.parm7(), geom.parm8(), geom.parm9(), geom.parm10());
      printf("Defining crystal j=%d, n=%d, i=%d, alveola=%d\n",j,n,i,num_al);
      solidCrystal[j]    = NULL;
      logicalCrystal[j]  = NULL;
      physicalCrystal[j] = NULL;
      solidCrystal[j]    = new G4Trap( geom.XtalName(), geom.parm0(), geom.parm1(), geom.parm2(), geom.parm3(), geom.parm4(),
                                       geom.parm5(),geom.parm6(), geom.parm7(), geom.parm8(), geom.parm9(), geom.parm10());
      logicalCrystal[j]  = new G4LogicalVolume( solidCrystal[j], CrystalMaterial, geom.XtalName(), 0,0,0);
      physicalCrystal[j] = new G4PVPlacement( transf,                  //its position
                                              geom.XtalName(),         //its name
                                              logicalCrystal[j],       //its logical volume
                                              physicalAlveola[num_al], //its mother
                                              false,                   //no boolean operat
                                              0);                      //copy number
      j++;
    }
    fgets(ligne, 1024, Geometry);
  }
  fclose(Geometry);
 
//----------------------------------------------ZONES SENSIBLES------------------------------- 

// Sensitive Detectors: Crystal
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
   
  if(!calorimeterSD)
  {
    calorimeterSD = new CalorimeterSD( "CalorSD",this );
    SDman->AddNewDetector( calorimeterSD );
  }
  
  for(int k = 0; k<nb_Xtal ; k++)
  {
    logicalCrystal[k]->SetSensitiveDetector(calorimeterSD);
  }
  
// Visualization attributes
  logicalWorld->SetVisAttributes (G4VisAttributes::GetInvisible());
 
#ifndef TB_2018
  if(visu%10 == 0)
  {
    for (int k=0;k<500;k++)  
    {
      logicalCrystal[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
    }
  }
  
  if(visu%100 - visu%10 == 0)
  {
    for (int k=500;k<900;k++)  
    {
      logicalCrystal[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
    }
  }
  
  if(visu%1000 - visu%100 == 0)
  {
    for (int k=900;k<1300;k++)  
    {
      logicalCrystal[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
    }
  }
  
  if(visu%10000 - visu%1000 == 0)
  {
    for (int k=1300;k<1700;k++)  
    {
      logicalCrystal[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
    }
  }
#else
  if(visu == 0)
  {
    for (int k=0;k<nb_Xtal;k++)  
    {
      logicalCrystal[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
    }
  }
#endif
 
  
  for (int k=0;k<nb_alveola*alveola_stack;k++)  
  {
    logicalAlveola[k]->SetVisAttributes (G4VisAttributes::GetInvisible());
  }
  
#ifndef TB_2018
  logicalPlateInfP->SetVisAttributes (G4VisAttributes::GetInvisible());
  
  if(VisAlu == 0)
  {
    logicalPlateInfCD->SetVisAttributes (G4VisAttributes::GetInvisible()); 
    logicalPlateInfCG->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalPlateInfP2->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalPlateInfP3->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE1CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE1CG->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE1CN->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE1EC->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE2CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE2CG->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE2CN->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE2EC->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE3CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE3CG->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE3CN->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE3EC->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE4CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE4CG->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE4CN->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE4EC->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE5CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE5CG->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE6CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE6CG->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE7CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE7CG->SetVisAttributes (G4VisAttributes::GetInvisible());
  
    logicalE8CD->SetVisAttributes (G4VisAttributes::GetInvisible());
    logicalE8CG->SetVisAttributes (G4VisAttributes::GetInvisible());
  
  }
#endif  

  G4VisAttributes* simpleBoxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  simpleBoxVisAtt->SetVisibility(true);

  printf("Normally, at this stage, we have created all the SM items\n");
//always return the physical World
  return physicalWorld;
}

//======================================================================
//---------------------------Messengers---------------------------------
//======================================================================
void DetectorConstruction::SetVisSM(G4int Choice)
{ 
  visu = Choice;       
}

void DetectorConstruction::SetVisAlu(G4String Choice)
{
  if(Choice == "on") 
  {
    VisAlu = 1;
  }
  else
  {
    VisAlu = 0;
  }
}

void DetectorConstruction::SetModAngles(G4String Choice)
{
  G4int iret;
  G4double loc_theta=0., loc_phi=0.;
  char cloc_unit[80];
  iret=sscanf(Choice,"%lf %lf %s",&loc_theta, &loc_phi, cloc_unit);
  printf("Rotate the matrix by %d %e %e xx%sxx : xx%sxx\n",iret,loc_theta,loc_phi, cloc_unit, Choice.data());
  G4String loc_unit=cloc_unit;
  if(iret==2) loc_unit="rad";
  if(loc_unit=="deg")
  {
    loc_theta*=deg;
    loc_phi*=deg;
  }
  ModTheta=ModTheta_ref+loc_theta;
  ModPhi=ModPhi_ref+loc_phi;
  UpdateGeometry();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructCalorimeter());
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
