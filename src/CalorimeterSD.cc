
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "CalorimeterSD.hh"

#include "EcalHit.hh"
#include "DetectorConstruction.hh"

#include "G4EmProcessSubType.hh"
#include "G4VProcess.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

CalorimeterSD::CalorimeterSD(G4String name, DetectorConstruction* det) : G4VSensitiveDetector(name)
{
  collectionName.insert("EcalCollection");
  fHitID = new G4int[1];
  fDetector=det;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

CalorimeterSD::~CalorimeterSD()
{
  delete [] fHitID;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CalorimeterSD::Initialize(G4HCofThisEvent *HCE)
{
  fEcalCollection = new EcalHitsCollection (SensitiveDetectorName,collectionName[0]);
  fHitID[0] = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool CalorimeterSD::ProcessHits(G4Step* aStep,G4TouchableHistory* ROhist)
{

  G4TouchableHistory* theTouchable = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
  G4VPhysicalVolume* physVol = theTouchable->GetVolume(); 

  G4double edep = aStep->GetTotalEnergyDeposit();
  const G4int nHit_max=1000; // No more than 1000 hits per step
  EcalHit* Hit[nHit_max];
  
  G4double stepl = 0.;
  G4Track *track=aStep->GetTrack();
  aStep->InitializeStep(track);
  if ((edep==0.)&&(stepl==0.)) return false;      

  const G4ParticleDefinition* particle = track->GetDefinition();
  const G4DynamicParticle* dynParticle = track->GetDynamicParticle();

  if (particle->GetPDGCharge() != 0.) stepl = aStep->GetStepLength();
  G4double mass=0.;
  if(particle) mass = particle->GetPDGMass();
// Retreive position, direction, energy
  G4String ParticleName = dynParticle-> GetParticleDefinition()->GetParticleName();
  const std::vector<const G4Track*>* secondaries = aStep->GetSecondaryInCurrentStep();
  G4ThreeVector pos0 = track->GetPosition();
  pos0.setY(pos0.y()-fDetector->PosY);
  pos0.setZ(pos0.z()+fDetector->PosX);
  G4ThreeVector dir0 = track->GetMomentumDirection();
  G4double kinE = dynParticle->GetKineticEnergy();
  G4double Time = track->GetGlobalTime();

// Add one collection at each step to forward to SLitrani
  int Xtal_number = 0;
  for(G4int i=0;i<nb_Xtal;i++)
  {
    if(physVol == fDetector->GetXtal(i))
    {
      Xtal_number = i;
      break;
    }
  }
  G4int nHit=0;
  Hit[nHit] = new EcalHit();
  //printf("Set dEdx in Volume %d : Edep: %e\n",Xtal_number, edep);
  Hit[nHit]->SetHit(Xtal_number,0, (G4float)Time, pos0, dir0, (G4float)edep);    
  fHitID[0] = fEcalCollection->insert(Hit[nHit]) - 1;
  //printf("in HitID %d\n",fHitID[0]);

  if (secondaries->size()>0)
  {
    for(unsigned int i=0; i<secondaries->size(); i++)
    {
      const G4Track *strack=secondaries->at(i);
      if (strack->GetParentID()>0)
      {   
        G4int pid = strack->GetParentID();
        mass = strack->GetDefinition()->GetPDGMass();
        kinE = strack->GetDynamicParticle()->GetKineticEnergy();
        Time = strack->GetGlobalTime()*ns;
        G4ThreeVector pos1 = strack->GetPosition()*mm;
        pos1.setY(pos1.y()-fDetector->PosY);
        pos1.setZ(pos1.z()+fDetector->PosX);
        G4ThreeVector dir1 = strack->GetMomentumDirection();
        G4String sName = strack->GetDynamicParticle()-> GetParticleDefinition()->GetParticleName();
        const G4VProcess* proc = strack->GetCreatorProcess();
        G4int type = proc->GetProcessSubType();
        if(type == fCerenkov)
        {
          if(nHit<nHit_max-1)
          {
            Hit[++nHit] = new EcalHit();
            //printf("Set Cerenkov in Volume %d : E= %e, WL: %f nm\n",Xtal_number, kinE, 1243.12/(kinE/eV));
            Hit[nHit]->SetHit(Xtal_number,1, Time, pos1, dir1, kinE);    
            fHitID[0] = fEcalCollection->insert(Hit[nHit]) - 1;
            //printf("in HitID = %d\n",fHitID[0]);
          }
        }
      }   
    }   
  }
 
  //theTouchable->MoveUpHistory();
 
  if (verboseLevel>0)
  G4cout << " New Calorimeter Hit on layer: " <<  Xtal_number << G4endl;
    
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CalorimeterSD::EndOfEvent(G4HCofThisEvent* HCE)
{
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection(HCID,fEcalCollection);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CalorimeterSD::clear()
{} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CalorimeterSD::DrawAll()
{} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void CalorimeterSD::PrintAll()
{
G4cout<<this->active<<G4endl;
G4cout<<this->SensitiveDetectorName<<G4endl;
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

