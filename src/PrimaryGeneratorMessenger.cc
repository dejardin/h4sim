
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "PrimaryGeneratorMessenger.hh"

#include "PrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorMessenger::PrimaryGeneratorMessenger( PrimaryGeneratorAction* Gun)
:Action(Gun)
{ 
  //gunParticle = new G4UIcmdWithAString("/gun/particule",this);
  //gunParticle->SetGuidance("Choose particle type");
  //gunParticle->SetGuidance("  Choice : e-(default), e+, gamma");
  //gunParticle->SetParameterName("particle",true);
  //gunParticle->SetDefaultValue("e-");
  //gunParticle->SetCandidates("e- e+ gamma");

  G4UIdirectory* uidir = new G4UIdirectory("/H4/");
  uidir->SetGuidance("H4 special commands") ;

  RndmCmd = new G4UIcmdWithAnInteger("/H4/RndmSeed",this);
  RndmCmd->SetGuidance("Choose random seed");
  RndmCmd->SetParameterName("Seed",true,true);
  RndmCmd->SetDefaultValue(1);

  beamtype = new G4UIcmdWithAString("/H4/beamtype",this);
  beamtype->SetGuidance("Choose beam type");
  beamtype->SetGuidance("  Choice : center(default), tb");
  beamtype->SetParameterName("BeamType",true);
  beamtype->SetDefaultValue("center");
  beamtype->SetCandidates("center testbeam");
  //  beamtype->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  beamprofile = new G4UIcmdWithAString("/H4/beamprofile",this);
  beamprofile->SetGuidance("Choose beam profile");
  beamprofile->SetGuidance("  Choice : flat(default), gauss, point");
  beamprofile->SetParameterName("BeamProfile",true);
  beamprofile->SetDefaultValue("flat");
  beamprofile->SetCandidates("flat gauss point");

// For 2018 TB :
  posXY = new G4UIcmdWithAString("/H4/SetPosXY",this);
  posXY->SetGuidance("Set X-Y crystal position (2018 TB)");
  posXY->SetParameterName("choice",false);
  posXY->SetDefaultValue("0. 0 cm");

// For 2023 TB :
  posEtaPhi = new G4UIcmdWithAString("/H4/SetPosEtaPhi",this);
  posEtaPhi->SetGuidance("Set eta-phi crystal position (with SM)");
  posEtaPhi->SetParameterName("choice",true);
  posEtaPhi->SetDefaultValue("1 1");

  beamxmin = new G4UIcmdWithADoubleAndUnit("/H4/beamxmin",this);
  beamxmin->SetGuidance("Set Beam X min (flat beam profile)");
  beamxmin->SetParameterName("beamxmin",true,true);
  beamxmin->SetDefaultUnit("mm");
  beamxmin->SetUnitCandidates("micron mm cm m km");

  beamxmax =  new G4UIcmdWithADoubleAndUnit("/H4/beamxmax",this);
  beamxmax->SetGuidance("Set Beam X max (flat beam profile)");
  beamxmax->SetParameterName("beamxmax",true,true);
  beamxmax->SetDefaultUnit("mm");
  beamxmax->SetUnitCandidates("micron mm cm m km");

  beamxmean =  new G4UIcmdWithADoubleAndUnit("/H4/beamxmean",this);
  beamxmean->SetGuidance("Set Beam X Mean (gaussian beam profile)");
  beamxmean->SetParameterName("beamxmean",true,true);
  beamxmean->SetDefaultUnit("mm");
  beamxmean->SetUnitCandidates("micron mm cm m km");

  beamxsigma =  new G4UIcmdWithADoubleAndUnit("/H4/beamxsigma",this);
  beamxsigma->SetGuidance("Set Beam X Sigma (gaussian beam profile)");
  beamxsigma->SetParameterName("beamxsigma",true,true);
  beamxsigma->SetDefaultUnit("mm");
  beamxsigma->SetUnitCandidates("micron mm cm m km");

  beamymin =  new G4UIcmdWithADoubleAndUnit("/H4/beamymin",this);
  beamymin->SetGuidance("Set Beam Y min (flat beam profile)");
  beamymin->SetParameterName("beamymin",true,true);
  beamymin->SetDefaultUnit("mm");
  beamymin->SetUnitCandidates("micron mm cm m km");

  beamymax =  new G4UIcmdWithADoubleAndUnit("/H4/beamymax",this);
  beamymax->SetGuidance("Set Beam Y max (flat beam profile)");
  beamymax->SetParameterName("beamymax",true,true);
  beamymax->SetDefaultUnit("mm");
  beamymax->SetUnitCandidates("micron mm cm m km");

  beamymean =  new G4UIcmdWithADoubleAndUnit("/H4/beamymean",this);
  beamymean->SetGuidance("Set Beam Y Mean (gaussian beam profile)");
  beamymean->SetParameterName("beamymean",true,true);
  beamymean->SetDefaultUnit("mm");
  beamymean->SetUnitCandidates("micron mm cm m km");

  beamysigma =  new G4UIcmdWithADoubleAndUnit("/H4/beamysigma",this);
  beamysigma->SetGuidance("Set Beam Y Sigma (gaussian beam profile)");
  beamysigma->SetParameterName("beamysigma",true,true);
  beamysigma->SetDefaultUnit("mm");
  beamysigma->SetUnitCandidates("micron mm cm m km");

  dx = new G4UIcmdWithADoubleAndUnit("/H4/dx",this);
  dx->SetGuidance("Set Dx on the front face of the xtal");
  dx->SetParameterName("dx",true,true);
  dx->SetDefaultUnit("mm");
  dx->SetUnitCandidates("micron mm cm m km");

  dy = new G4UIcmdWithADoubleAndUnit("/H4/dy",this);
  dy->SetGuidance("Set Dy on the front face of the xtal");
  dy->SetParameterName("dy",true,true);
  dy->SetDefaultUnit("mm");
  dy->SetUnitCandidates("micron mm cm m km");

  dtheta = new G4UIcmdWithADouble("/H4/dtheta",this);
  dtheta->SetGuidance("Set Dtheta of the beam");
  dtheta->SetParameterName("dtheta",true,true);

  dphi = new G4UIcmdWithADouble("/H4/dphi",this);
  dphi->SetGuidance("Set Dphi of the beam");
  dphi->SetParameterName("dphi",true,true);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorMessenger::~PrimaryGeneratorMessenger()
{

  //delete gunParticle;
  delete RndmCmd;
  delete beamtype;
  delete beamprofile;
  delete posXY;
  delete posEtaPhi;
  delete beamxmin;
  delete beamxmax;
  delete beamxmean;
  delete beamxsigma;
  delete beamymin;
  delete beamymax;
  delete beamymean;
  delete beamysigma;
  delete dx;
  delete dy;
  delete dtheta;
  delete dphi ;

  //  delete Balai_Cmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorMessenger::SetNewValue( G4UIcommand* command, G4String newValue)
{
  //if( command == gunParticle )
  // { Action->SetGunParticle(newValue);}
  if( command == RndmCmd )
   { Action->SetRndmSeed(RndmCmd->GetNewIntValue(newValue));}
  if( command == beamtype )
   { Action->SetBeamType(newValue);}
  if( command == beamprofile )
   { Action->SetBeamProfile(newValue);}
  if( command == posXY)
    { Action->SetposXY(newValue);}
  if( command == posEtaPhi )
    { Action->SetposEtaPhi(newValue);}
//   if( command == xoff )
//    { Action->SetXOff(xoff->GetNewDoubleValue(newValue));}
//   if( command == yoff )
//    { Action->SetYOff(yoff->GetNewDoubleValue(newValue));}
  if( command == dtheta )
   { Action->SetDTheta(dtheta->GetNewDoubleValue(newValue));}
  if( command == dphi )
   { Action->SetDPhi(dphi->GetNewDoubleValue(newValue));}
  if( command == dx )
   { Action->SetDeltaX(dx->GetNewDoubleValue(newValue));}
  if( command == dy )
   { Action->SetDeltaY(dy->GetNewDoubleValue(newValue));}
  if( command == beamxmin )
   { Action->SetBeamXMin(beamxmin->GetNewDoubleValue(newValue));}
  if( command == beamxmax )
   { Action->SetBeamXMax(beamxmax->GetNewDoubleValue(newValue));}
  if( command == beamxmean )
   { Action->SetBeamXMean(beamxmean->GetNewDoubleValue(newValue));}
  if( command == beamxsigma )
   { Action->SetBeamXSigma(beamxsigma->GetNewDoubleValue(newValue));}
  if( command == beamymin )
   { Action->SetBeamYMin(beamymin->GetNewDoubleValue(newValue));}
  if( command == beamymax )
   { Action->SetBeamYMax(beamymax->GetNewDoubleValue(newValue));}
  if( command == beamymean )
   { Action->SetBeamYMean(beamymean->GetNewDoubleValue(newValue));}
  if( command == beamysigma )
   { Action->SetBeamYSigma(beamysigma->GetNewDoubleValue(newValue));}


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

