#include "Rotation.hh"

Rotation::Rotation()
{
}

Rotation::~Rotation()
{
}

//--------------------------------------------------------------------
//-------------------------Rotation Matrix-------------------------
//--------------------------------------------------------------------

void Rotation::position_rot(double px , double py , double pz ,
                            double thx, double thy , double thz )
{

  XX = sin (thx)*cos(px);
  XY = sin (thy)*cos(py);
  XZ = sin (thz)*cos(pz);
  YX = sin (thx)*sin(px);
  YY = sin (thy)*sin(py);
  YZ = sin (thz)*sin(pz);
  ZX = cos(thx);
  ZY = cos(thy);
  ZZ = cos(thz);

}

double  Rotation::XXr()
{
  return XX;
} 
 
double  Rotation::XYr()
{
  return XY;
}
 
double  Rotation::XZr()
{
  return XZ;
} 
 
double  Rotation::YXr()
{
  return YX;
}
 
double  Rotation::YYr()
{
  return YY;
} 
 
double  Rotation::YZr()
{
  return YZ;
}
 
double  Rotation::ZXr()
{
  return ZX;
} 
 
double  Rotation::ZYr()
{
  return ZY;
}
 
double  Rotation::ZZr()
{
  return ZZ;
}
 
