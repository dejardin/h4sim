
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorMessenger.hh"

#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::DetectorMessenger( DetectorConstruction* Det)
:Detector(Det)
{ 
  detDir = new G4UIdirectory("/ECAL/");
  detDir->SetGuidance(" detector control.");
      
  Vis_SM_Cmd = new G4UIcmdWithAnInteger("/ECAL/",this);
  Vis_SM_Cmd->SetGuidance("Choose  : 0000 0001 0010 0100 1000 1111");
  Vis_SM_Cmd->SetParameterName("choice",false);
  Vis_SM_Cmd->SetDefaultValue(1111);
  Vis_SM_Cmd->AvailableForStates(G4State_Idle);
  
  Vis_Alu_Cmd = new G4UIcmdWithAString("/ECAL/Alu",this);
  Vis_Alu_Cmd->SetGuidance("Aluminium Plates");
  Vis_Alu_Cmd->SetParameterName("choice",false);
  Vis_Alu_Cmd->SetCandidates("on off");
  Vis_Alu_Cmd->AvailableForStates(G4State_Idle);
  
  Set_ModAngles_Cmd = new G4UIcmdWithAString("/ECAL/SetModAngles",this);
  Set_ModAngles_Cmd->SetGuidance("Rotate in Theta and Phi (around Y and Z direction");
  Set_ModAngles_Cmd->SetParameterName("choice",false);
  Set_ModAngles_Cmd->SetDefaultValue("0. 0. deg");
  Set_ModAngles_Cmd->AvailableForStates(G4State_Idle);
  
  UpdateCmd = new G4UIcmdWithoutParameter("/ECAL/update",this);
  UpdateCmd->SetGuidance("Update calorimeter geometry.");
  UpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  UpdateCmd->SetGuidance("if you changed geometrical value(s).");
  UpdateCmd->AvailableForStates(G4State_Idle);
      
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorMessenger::~DetectorMessenger()
{
  delete Vis_SM_Cmd;
  delete Vis_Alu_Cmd;
  delete Set_ModAngles_Cmd;
  delete UpdateCmd;
  delete detDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == Vis_SM_Cmd )
   { Detector->SetVisSM(Vis_SM_Cmd->GetNewIntValue(newValue));}
   
  if( command == Vis_Alu_Cmd )
   { Detector->SetVisAlu(newValue);}
  
  if( command == Set_ModAngles_Cmd )
   { Detector->SetModAngles(newValue);}
  
  if( command == UpdateCmd )
   { Detector->UpdateGeometry(); }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
